import React, { Component } from 'react';
// import logo from './/logo.svg';
import './App.css';
import Home from './Components/Home/Home';
import Category from './Components/Category/Category';
import HeaderTop from './Components/HeaderTop/HeaderTop';
import HeaderBottom from './Components/HeaderBottom/HeaderBottom';
import HeaderSlider from './Components/HeaderSlider/HeaderSlider';
import NewArrive from './Components/NewArrive/NewArrive';
import PopularBrands from './Components/PopularBrands/PopularBrands';
import NewCollection from './Components/NewCollection/NewCollection';
import FinalSale from './Components/FinalSale/FinalSale';
import Event from './Components/Event/Event';
import Footer from './Components/Footer/Footer';
import Newsletter from './Components/Newsletter/Newsletter';
import Login from "./Components/Login/Login";
import ExpandCategoris from "./Components/ExpandCategoris/ExpandCategoris";
import Filter from "./Components/Filter/Filter";
import Paginations from "./Components/Paginations/Paginations";
import Breadcrumbing from "./Components/Breadcrumbing/Breadcrumbing";
import Wishlist from "./Components/Wishlist/Wishlist";
import Register from "./Components/Register/Register";
import Signwith from "./Components/Signwith/Signwith";
import CartEmpty from "./Components/CartEmpty/CartEmpty";
import WishlistEmpty from "./Components/WishlistEmpty/WishlistEmpty";
import ForgitPassword from "./Components/ForgitPassword/ForgitPassword";
import EmailSent from "./Components/EmailSent/EmailSent";
import Checkout from "./Components/Checkout/Checkout";
import Cart from "./Components/Cart/Cart";
import SingleProducts from "./Components/SingleProducts/SingleProducts";
import Quantity from "./Components/Quantity/Quantity";
import Reviews from "./Components/Reviews/Reviews";
import OrderRecived from "./Components/OrderRecived/OrderRecived";
import UserSetting from "./Components/UserSetting/UserSetting";
import PaymentMethod from "./Components/PaymentMethod/PaymentMethod";
import AddPayment from "./Components/AddPayment/AddPayment";
import PaymentEmpty from "./Components/PaymentEmpty/PaymentEmpty";
import OrdersHistory from "./Components/OrdersHistory/OrdersHistory";
import AddShipping from "./Components/AddShipping/AddShipping";
import Copyright from "./Components/Copyright/Copyright";
import PopularProducts from "./Components/PopularProducts/PopularProducts";
import TopRating from "./Components/TopRating/TopRating";
import OrderEmpty from "./Components/OrderEmpty/OrderEmpty";
import UserProfile from "./Components/UserProfile/UserProfile";
import AddressEmpty from "./Components/AddressEmpty/AddressEmpty";
import ReviewsRating from "./Components/ReviewsRating/ReviewsRating";
import ProductItemGrid from "./Components/ProductItemGrid/ProductItemGrid";
import ProductItem from "./Components/ProductItem/ProductItem";
import ListProducts from "./Components/ListProducts/ListProducts";
import GridProducts from "./Components/GridProducts/GridProducts";
import ShopSort from "./Components/ShopSort/ShopSort";
import Terms from "./Components/Terms/Terms";
import Shop from "./Components/Shop/Shop";
import Outfit from "./Components/Outfit/Outfit";
import langs from './lang';
import LocalizedStrings from 'react-localization';
import { Switch, Route } from 'react-router-dom';

class App extends Component {
  constructor() {
    super();
    let strings = new LocalizedStrings(langs);
    this.onLangChange = this.onLangChange.bind(this)
    this.state = {
      strings: strings,
      lang: sessionStorage.getItem("lang") ? (sessionStorage.getItem("lang")) : ('en'),
      cartLength: undefined
    }
  }
  componentDidMount() {
    if (!sessionStorage.getItem("lang")) {
      sessionStorage.setItem("lang", 'en')
    }
  }

  onLangChange(language) {
    sessionStorage.setItem("lang", language)
    let strings = this.state.strings
    strings.setLanguage(language)
    this.setState({ strings: strings, lang: language })
    if ("activeElement" in document)
      document.activeElement.blur();
  }
  render() {
    return (

      <div>
        <React.Fragment>
          {this.state.lang === "ar" ? (
            <React.Fragment>
              <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/rtl.css'} />
              <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/style_ar.css'} />
            </React.Fragment>
          ) : (
              <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/style.css'} />
            )}
          <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/common_style.css'} />


          <ExpandCategoris strings={this.state.strings} />
          <HeaderTop onLangChange={this.onLangChange} strings={this.state.strings} />
          <HeaderBottom onLangChange={this.onLangChange} strings={this.state.strings} />
          <Switch>
            <Route path='/terms' render={() => <Terms strings={this.state.strings} />} />

            <Route path='/shop' render={() => <Shop strings={this.state.strings} />} />

            <Route path='/outfit' render={() => <outfit strings={this.state.strings} />} />

            <Route path='/AddPayment' render={() => <AddPayment strings={this.state.strings} />} />
            

            <Route path="/listproducts" component={ListProducts} />
            <Route path="/reviewrating" component={ReviewsRating} />
            <Route path='/AddressEmpty' render={() => <AddPayment strings={this.state.strings} />} />
            <Route path="/AddressEmpty" component={AddressEmpty} />
            
            <Route path='/UserProfile' render={() => <UserProfile strings={this.state.strings} />} />
            
            
            <Route path="/orderempty" component={OrderEmpty} />
            <Route path="/addshipping" component={AddShipping} />
            <Route path='/ordershistory' render={() => <ordershistory strings={this.state.strings} />} />
           
            
            <Route path="/paymentempty" component={PaymentEmpty} />
           
            <Route path="/Paymentmethod" component={PaymentMethod} />
            <Route path="/usersetting" component={UserSetting} />
            <Route path="/orderrecived" component={OrderRecived} />
            <Route path="/reviews" component={Reviews} />
            <Route path='/Cart' render={() => <Cart strings={this.state.strings} />} />
            
            <Route path="/checkout" component={Checkout} />
            <Route path="/EmailSent" component={EmailSent} />
            <Route path="/ForgitPassword" component={ForgitPassword} />
            <Route path="/Wishlistempty" component={WishlistEmpty} />
            <Route path="/Cartempty" component={CartEmpty} />
            <Route path="/Register" component={Register} />
            <Route path="/Wishlist" component={Wishlist} />
            <Route path="/GridProducts" component={GridProducts} />
            <Route path="/singleshop" component={SingleProducts} />
            <Route path="/Login" render={() => <Login strings={this.state.strings} />} />
            <Route path="/" render={() => <Home strings={this.state.strings} />} />



          </Switch>
          <Newsletter strings={this.state.strings} />
          <Footer strings={this.state.strings} />
          <Copyright strings={this.state.strings}/>
        </React.Fragment>

      </div>
    );
  }
}

export default App;
