import React from "react";
import { ListGroupItem, ListGroup, Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Button, Tab, NavItem, Nav } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import UserSetting from "../UserSetting/UserSetting";
class PaymentEmpty extends React.Component {
    render() {
        return (
            <Grid>



                <div className="payment__empty">
                    <Row>
                        <UserSetting />
                        <div className="text">
                            <Col md={9}>
                                <div className="payment__method-bg">
                                    <div>
                                        <div className="Payment__methods__title">
                                            <h3> Payment Method</h3>
                                        </div>




                                        <hr className="Payment__methods-hr" />
                                    </div>
                                    <Row>
                                        <Col md={8} mdOffset={2}>
                                            <div className="payment-empty__holder">
                                                <h3 class="emptytitle"><span >Unfortunately </span>, No Payment Methods Yet</h3>
                                                <img class="emptycart__img" src={require("../../img/payment/payment-empty.png")} />
                                                <div className="payment-empty__buttons">
                                                    <a href="/"><button type="button" class="payment-empty__add btn">Add New Payment Method</button></a>
                                                </div>
                                            </div>


                                        </Col>
                                    </Row>

                                </div>

                            </Col>
                        </div>
                    </Row>
                    <hr />
                </div>
                <Features />
            </Grid>

        );
    }
}
export default PaymentEmpty;