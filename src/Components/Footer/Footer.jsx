import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import ScrollAnimation from 'react-animate-on-scroll';
class Footer extends React.Component {

render() {
    
  return (
    <div>


    <div className="footer">
    <div className="container">
      <div className="row">
        <div className="col-md-4">
        <ScrollAnimation animateIn="fadeInLeft"
               			animateOnce={true}>
          <div className="footer-logo">
            <img src={require("../../img/Home/Logo-footer.png")} />
          </div>
   
          <ul className="social-network social-circle">
            <li><a href="#" className="icoFacebook" title="Facebook"><i className="fa fa-facebook" /></a></li>
            <li><a href="#" className="icoTwitter" title="Twitter"><i className="fa fa-twitter" /></a></li>
            <li><a href="#" className="icoyoutube" title="youtube"><i className="fa fa-youtube" /></a></li>

            <li><a href="#" className="icoinstgram" title="Dribble"><i className="fa fa-instagram" /></a></li>
          </ul>
          </ScrollAnimation>

        </div>
        <div className="col-md-8">
        <ScrollAnimation animateIn="fadeInRight"
               			animateOnce={true}>
          <div className="row">
            <div className="col-md-3">
              <div className="footer__menu-holder footer-border">
                <div className="footer-menu-title">
                  <h3> {this.props.strings.catee}</h3>
                </div>
                <ul className="footer__menu">
                  <li className="footer__menu-link"><a href="/shop"> {this.props.strings.Yours} </a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.TShirts}</a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.Shirts}</a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.Shoes} </a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.Accessorie} </a></li>

                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="footer__menu-holder footer-border">
                <div className="footer-menu-title">
                <h3>{this.props.strings.duct} </h3>
                </div>
                <ul className="footer__menu">
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.newarr}</a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.popular}</a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.Trending}</a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.Tbrands}</a></li>
                  <li className="footer__menu-link"><a href="/shop">{this.props.strings.Bestie}</a></li>

                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="footer__menu-holder footer-border">
                <div className="footer-menu-title">
                <h3>{this.props.strings.acc} </h3>
              

                </div>
                <ul className="footer__menu">
                  <li className="footer__menu-link"><a href="/userprofile"> {this.props.strings.prof}</a></li>
                  <li className="footer__menu-link"><a href="/ordershistory">{this.props.strings.order}</a></li>
                  <li className="footer__menu-link"><a href="/wishlist">{this.props.strings.wishhy}</a></li>
                  <li className="footer__menu-link"><a href="/paymentmethod">{this.props.strings.paymentfot}</a></li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="footer__menu-holder ">
                <div className="footer-menu-title">
                <h3> {this.props.strings.addr}</h3>
                </div>
                {/* <div className="footer-caption">
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing<br /> elit, sed do eiusmod.</p>
          </div> */}
          <div className="footer_information">
            <div className="footer-address">
              <h3>{this.props.strings.district}
              </h3>
            </div>
            <br/>
            <div className="footer-phone">
              <h3>
              {this.props.strings.call}
              
              </h3></div>
          </div>
              </div>
            </div>
          </div>
          </ScrollAnimation>

        </div>
        <div className="footer__payment">
          {/* <img src={require("../../img/Homepage/footer/PaypalLogo.png")} />
          <img src={require("../../img/Homepage/footer/VisaLogo.png")} />
          <img src={requdire("../../img/Homepage/footer/MastercardLogo.png")} /> */}
        </div>
      </div>
    </div>
  </div>
  </div>
      );
  }
}

export default Footer;
