import React from "react";
import { Row, Col, Grid ,FormGroup,Form,FormControl ,ControlLabel,Checkbox} from 'react-bootstrap'
import Signwith from "../Signwith/Signwith";

class Login extends React.Component {
	
	render() {
             
		return (

<div>
          
        <div className="Login">
        <Grid>
        <div className="Login-bg">

            <Row>
                <Col md={6}>
                <div className="Login-left">
                <h3>{this.props.strings.log}</h3>
                <div className="Login-caption">

                <p>{this.props.strings.get} </p>
                </div>
                <Form horizontal>
  <FormGroup controlId="formHorizontalEmail">
  
      <FormControl type="email" placeholder="Username" />
  </FormGroup>
  <FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="password" placeholder="Password" />
</FormGroup>
<Col>
      <Checkbox>{this.props.strings.rem}</Checkbox>
      <a  className="Login-Forgit" href="#">{this.props.strings.pass} </a>

    </Col>
    <button type="button" class="login_create btn btn-secondry">{this.props.strings.log}</button>
<div className="Login-hint">
<p>{this.props.strings.agree} <a href="#">{this.props.strings.term} </a>{this.props.strings.us} </p>
</div>
  </Form>
  </div>

                </Col>
                <Col md={6}>
                <div className="Login-right">
                <h3>{this.props.strings.reg} </h3>
                <div className="Login-caption">

              <p>{this.props.strings.community}</p>
              </div>
              <form class="form-horizontal">
                <img src={require("../../img/Home/logo.png")}/>
                <div className="Login-content">
                <p>{this.props.strings.loreem}  </p>
</div>
<a href="/Register"><button type="button" class="login_create btn btn-secondry">{this.props.strings.creat}</button></a>
</form>
                </div>
               
                </Col>
                <div className="Login-hr clearfix">
                 <hr/>
                 </div>
                 <Signwith/>

             </Row>
</div>
        </Grid>

  </div>
       
        

      
        </div>

       );
    }
  }
  
  export default Login;
  