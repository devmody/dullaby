import React from "react";
import { Row, Col, MenuItem, DropdownButton, ButtonToolbar, Button, Grid, ToggleButtonGroup, ToggleButton } from 'react-bootstrap'
import Filter from "../Filter/Filter";
import ProductItem from "../ProductItem/ProductItem";
import ProductItemGrid from "../ProductItemGrid/ProductItemGrid";
import ShopSort from "../ShopSort/ShopSort";
import PopularProducts from "../PopularProducts/PopularProducts";
import GridProducts from "../GridProducts/GridProducts";
import Paginations from "../Paginations/Paginations";
import ListProducts from "../ListProducts/ListProducts";

class Shop extends React.Component {

    constructor() {
        super()
        this.settingView=this.settingView.bind(this);
        this.state = {
            view: 'grid',
        }
    }

    settingView(newView){
        this.setState({view:newView})
    }
    render() {
        return (


            <div className="products">


            <div className="collection-header">


<div className="collection-hero">
<div className="collection-hero__image">
</div>
<div className="collection-hero__title-wrapper">
<h1 className="collection-hero__title page-width">{this.props.strings.wom}</h1>
</div>
</div>

</div>
                <Grid>
                <Row>
                    <Col md={3}>
                        <Filter {...this.props} />
                    </Col>
                   
                    <Col md={9}>
                        <ShopSort settingView={this.settingView} {...this.props} />

                       
                            {this.state.view ==='grid'?
                            <GridProducts {...this.props}/>
                            :
                            <ListProducts {...this.props}/>
                            }
                        

                    </Col>
                    <Paginations {...this.props}/>
                    </Row>

                </Grid>

                <div>
                    <PopularProducts {...this.props} />
                </div>

            </div>
        );
    }
}
export default Shop;