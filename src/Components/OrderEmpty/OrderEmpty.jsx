import React from "react";
import { Row, Col ,Grid  , Button} from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import UserSetting from "../UserSetting/UserSetting";

class OrderEmpty extends React.Component {
	 
	render() {
      
		return (



            <Grid>




            <div className="order-empty payment__empty">
                <Row>

                    <UserSetting {...this.props}/>
                    <div className="text">
                        <Col md={9}>
                            <div className="payment__method-bg">
                                <div>
                                    <div className="Payment__methods__title">
                                        <h3> Orders History</h3>
                                    </div>
 
                                    <hr className="Payment__methods-hr" />
                                <Row>

                                    <Col md={8} mdOffset={2}>
                                    <div className="order-history__content">

                          <h1> <span className="order-empty__color">Unfortunately  </span>! No orders Yet </h1>  

                   <img src={require("../../img/payment/empty-order.png")} />
                   <div className="order-empty_btn">

                     <Button className="order-empty__start" bsStyle="warning">Start Shopping</Button>
</div>

                                    </div>

                                    </Col>
                                </Row>
                            </div>
                            </div>
                        </Col>
                    </div>
                </Row>
                <hr />
            </div>
            {/* <Features /> */}
        </Grid>

       );
    }
  }
  
  export default OrderEmpty;
  