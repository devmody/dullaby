import React from "react";
import HeaderTop from '../HeaderTop/HeaderTop'
import Headerbottom from "../HeaderBottom/HeaderBottom";
import HeaderSlider from "../HeaderSlider/HeaderSlider";
import Category from "../Category/Category";
import NewArrive from "../NewArrive/NewArrive";
import PopularBrands from "../PopularBrands/PopularBrands";
import NewCollection from "../NewCollection/NewCollection";
import FinalSale from "../FinalSale/FinalSale";
import Event from "../Event/Event";
import Footer from "../Footer/Footer";
import Features from "../Features/Features";
import Newsletter from "../Newsletter/Newsletter";
import ExpandCategoris from "../ExpandCategoris/ExpandCategoris";
import Copyright from "../Copyright/Copyright";
import PopularProducts from "../PopularProducts/PopularProducts";
import ScrollAnimation from 'react-animate-on-scroll';
import langs from '../../lang';

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                
                <HeaderSlider {...this.props}/>
                
                <Category {...this.props}/>
                <NewArrive {...this.props}/>                
                <div className="ee">

                <PopularBrands {...this.props}/>
                <NewCollection {...this.props}/>
                <FinalSale {...this.props}/>
                <Event {...this.props}/>
                <PopularProducts {...this.props}/>
                </div>
                <Features {...this.props}/>       
            </React.Fragment>
        );
    }
}
  
export default Home;
  