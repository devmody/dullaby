import React from "react";
import { Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Tabs, Tab,NavItem,Nav } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Signwith from "../Signwith/Signwith";

class checkout extends React.Component {

    render() {
        return (

            <Grid>
                <Breadcrumbing />

                <div className="checkout">

                    <div className="checkout-track">
                    <Col md={6} mdOffset={3}>

                    <Row>

                            <li> <i class="material-icons">
                                check
</i>
                            </li>

                            <li className="checkout-track-title"> Shopping </li>
                
                            <li> <i class="material-icons">
                                check
</i>
                            </li>

                            <li className="checkout-track-title"> Shipping Info & Payment </li>

                    

                         <div className="checkout-track">
                         <li className="checkout-track-title circle"> 3</li>


                            <li className="checkout-track-title confirm"> Confirmation</li>
                            </div>

                        </Row>
                        </Col>

                    </div>

                    <Col md={7}>
                        <div className="checkout-left">
                            <div className="checkout-title">
                                <h3>     bILLING & SHIPPING INFO  </h3>
                                <p> Fill all the following forms with requiring data</p>

                            </div>
                            <Row>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="First Name*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Last Name*" />
                                    </FormGroup>
                                </Col>
                                <Col md={12}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Address 1*" />
                                    </FormGroup>
                                </Col>
                                <Col md={12}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Address 2*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <DropdownButton title="Country" id="Country">
                                        <MenuItem eventKey="1">Action</MenuItem>
                                        <MenuItem eventKey="2">Another action</MenuItem>
                                        <MenuItem eventKey="3">Something else here</MenuItem>
                                        <MenuItem divider />
                                        <MenuItem eventKey="4">Separated link</MenuItem>
                                    </DropdownButton>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="City / Town*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Postal Code*" />
                                    </FormGroup>
                                </Col>

                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Mobile Number*" />
                                    </FormGroup>
                                </Col>
                                <Col md={12}>
                                    <Checkbox>Ship to the same address</Checkbox>

                                </Col>
<div className="checkout-diffrent clearfix">
                                <h3>  ship to a different address ?</h3>

                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="First Name*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Last Name*" />
                                    </FormGroup>
                                </Col>
                                <Col md={12}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Address 1*" />
                                    </FormGroup>
                                </Col>
                                <Col md={12}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Address 2*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Country*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="City / Town*" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Postal Code*" />
                                    </FormGroup>
                                </Col>

                                <Col md={6}>

                                    <FormGroup controlId="formHorizontalEmail">

                                        <FormControl type="text" placeholder="Mobile Number*" />
                                    </FormGroup>
                                </Col>
                                <Col md={12}>
                                    <Checkbox>Ship to the same address</Checkbox>

                                </Col>

                                </div>
                            </Row>


                        </div>
                    </Col>
                    <Col md={5}>
                        <div className="checkout-right">
                            <div className="checkout-title">
                                <h3>    paytment methods </h3>
                                <p>We accept all kind of payment methods</p>
                            

 <Tab.Container id="left-tabs-example" defaultActiveKey="first">
  <Row>
    <Col sm={12}>
      <Nav bsStyle="pills" stacked>
        <NavItem eventKey="first">  
         <img title="1" src={require("../../img/checkout/credit.png")} />
<p> Credit Card </p></NavItem>
        <NavItem eventKey="second"><img title="1" src={require("../../img/checkout/paypal.png")} />
<p>paypal</p></NavItem>
      </Nav>
    </Col>
    <Row>
    <Col sm={12}>
      <Tab.Content animation>
        <Tab.Pane eventKey="first">
        
     
                                        <Col md={12}>

                                            <FormGroup controlId="formHorizontalEmail">

                                                <FormControl type="text" placeholder="Card Number" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={9}>

                                            <FormGroup controlId="formHorizontalEmail">

                                                <FormControl className="card-Holder" type="text" placeholder="Card Holder*" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={3}>

                                            <FormGroup controlId="formHorizontalEmail">

                                                <FormControl className="user-cvv" type="text" placeholder="CVV*" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>

                                            <DropdownButton title="Expire Month" id="ExpireMonth">
                                                <MenuItem eventKey="1">Action</MenuItem>
                                                <MenuItem eventKey="2">Another action</MenuItem>
                                                <MenuItem eventKey="3">Something else here</MenuItem>
                                                <MenuItem divider />
                                                <MenuItem eventKey="4">Separated link</MenuItem>
                                            </DropdownButton>
                                        </Col>

                                        <Col md={6}>

                                            <DropdownButton title="Expire Year" id="ExpireYear">
                                                <MenuItem eventKey="1">Action</MenuItem>
                                                <MenuItem eventKey="2">Another action</MenuItem>
                                                <MenuItem eventKey="3">Something else here</MenuItem>
                                                <MenuItem divider />
                                                <MenuItem eventKey="4">Separated link</MenuItem>
                                            </DropdownButton>
                                        </Col>
                                        <Col md={12}>
                                        <div className="checkout-payment">
                                            <img src={require("../../img/checkout/payment.png")} />
</div>
                                        </Col>
                                        
                                        </Tab.Pane>
        <Tab.Pane eventKey="second">Tab 2 content</Tab.Pane>

                <Tab.Pane eventKey="third">Tab 2 content</Tab.Pane>

      </Tab.Content>
    </Col>
    </Row>
  </Row>
</Tab.Container>

                            </div>
                        </div>


                        <br/>
                        <div className="checkout__total ">
                            <h3>  order total </h3>

                            <div className="checkout__total-numberholder">
                                <h4 className="checkout__total-number"> 1440 </h4>
                                <p className="checkout__total-currncy"> EGP </p>


                            </div>
                            <div className="checkout__total-caption">

                                <p>Free Shipping </p>
                                <p>25% Coupon Discount </p>

                            </div>
                            <div className="checkout__total-order">
                                <a href="/"><button type="button" class="login_create btn btn-secondry">  Place Order</button></a>

                            </div>
                        </div>

                    </Col>
                </div>




            </Grid>

        );
    }
}

export default checkout;
