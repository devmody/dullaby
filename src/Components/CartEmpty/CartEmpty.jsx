import React from "react";
import { Row, Col, Grid ,FormGroup,Form,FormControl ,ControlLabel,Checkbox,MenuItem,DropdownButton,ButtonToolbar} from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";

class CartEmpty extends React.Component {
	
	 render() {     
		return (

            <Grid>
            <Breadcrumbing/>

   <div className="cartempty">
   <Features/>
   <hr/>

<Row>
            <Col md={8} mdOffset={2}>

       <h3 className="emptytitle">Your Shopping Cart Is Empty </h3>
       <img  class="emptycart__img" src={require("../../img/cart/emptycart.png")} />
       <br/>

       <a href="#"><button type="button" class="empty-cart__continue btn ">Continue Shopping</button></a>
       <br/>
       <a href="/"><button type="button" class="empty-cart__start btn">Start From Beginning</button></a>

          </Col>
          </Row>
          
          </div>

          </Grid>

       );
    }
  }
  
  export default CartEmpty;
  