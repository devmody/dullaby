import React from "react";
import { Row, Col ,Grid ,ToggleButton,ButtonToolbar,ToggleButtonGroup,Button} from 'react-bootstrap'
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Quantity from "../Quantity/Quantity";
import InputNumber from 'rc-input-number';
import Features from "../Features/Features";

class OrderRecived extends React.Component {
    
render() {
return (
<div className="cart order-recived">
<Breadcrumbing/>

<Grid>
    <hr/>

       <div className="checkout-track container">
                    <Col md={6} mdOffset={3}>


                            <li> <i class="material-icons">
                                check
</i>
                            </li>

                            <li className="checkout-track-title"> Shopping </li>
                
                            <li> <i class="material-icons">
                                check
</i>
                            </li>

                            <li className="checkout-track-title"> Shipping Info & Payment </li>

                     <li> <i class="material-icons">
                                check
</i>
                            </li>

                            <li className="checkout-track-title"> Confirmation </li>


                   

                        </Col>

                    </div>
<div className="orderrecived__bg">
    <div className="order-recived_details">
    <h1> <span className="order-recived_details-thannk">THANK YOU !</span> Your order has been received </h1>
    <p className="will-get"> Will get reviewed and ship it as soon as possible </p>
    <Row>
    <Col md={10} mdOffset={1}>
<Col md={3}>
<div className="Order__Order-Number">
<h3> Order Number </h3>
<p> #23846</p>
</div>
</Col>

<Col md={3}>

<div className="Order__order-date">
<h3> Order Date </h3>
<p> 14 August 2018</p>
</div>
</Col>

<Col md={3}>

<div className="Order__total-cost">
<h3>  TOTAL COST </h3>
<p> 1440 egp</p>
</div>
</Col>

<Col md={3}>

<div className="Order__payment-method">
<h3> Payment Method </h3>
<p> Direct bank transfer</p>
</div>
</Col>

<div className="orderrecevied__hit">
<p> Make your payment directly into our bank account. Pleae use your Order ID as the payment 
reference. Your order wont be shipped until the funds have cleared in out account </p>
</div>
</Col>
</Row>
</div>
<div className="orderreciver__details">
<h3> Your order details </h3>
</div>
<div className="orderrecived-holder">
<div className="cart-bg">
            <div className="table">
                <div className="row header bg-orders">
                    <div className="cell product">
                        Product
                    </div>
                    <div className="cell Price">
                        Price
                    </div>
                    <div className="cell Quantity">
                        Quantity
                    </div>
                    <div className="cell Total-Price">
                        Total Price
                    </div>
                    
                </div>
                {/*  You can make map from here */}
                <div className="row cart__item-one">
                    <div className="cell" data-title="Name">
                        <div className="cart-img col-md-3">
                            <span> <img src={require("../../img/cart/shirt.png")} />  </span>
                        </div>
                        <div className="cart-name col-md-9">
                            <a href="#">  Tee shirt homme manches courtes encolure gr/m2 100% coton KARIBAN K357.</a>
                            <div className="cart-category">
                                <p> Category :<span class="cart__category-type">Men - Shirts  </span></p>
                                <p> Colour :<span class="cart__colour"> Blue  </span></p>
                                <p> size :<span class="cart__size"> XXl  </span></p>
                            </div>
                        </div>
                    </div>
                    <div className="cart__price cell" data-title="Age">
                        180 EGP
                    </div>
                    <div className="cart__quantity cell" data-title="Occupation">
                        <div>
                            2
                        </div>
                    </div>
                    <div className="cart__total-price cell" data-title="Location">
                        360 EGP
                    </div>
                   
                </div>
               
                {/*  End map */}

                  {/*  You can make map from here */}
                  <div className="row cart__item-one">
                    <div className="cell" data-title="Name">
                        <div className="cart-img col-md-3">
                            <span> <img src={require("../../img/cart/shirt.png")} />  </span>
                        </div>
                        <div className="cart-name col-md-9">
                            <a href="#">  Tee shirt homme manches courtes encolure gr/m2 100% coton KARIBAN K357.</a>
                            <div className="cart-category">
                                <p> Category :<span class="cart__category-type">Men - Shirts  </span></p>
                                <p> Colour :<span class="cart__colour"> Blue  </span></p>
                                <p> size :<span class="cart__size"> XXl  </span></p>
                            </div>
                        </div>
                    </div>
                    <div className="cart__price cell" data-title="Age">
                        180 EGP
                    </div>
                    <div className="cart__quantity cell" data-title="Occupation">
                        <div>
                            2
                        </div>
                    </div>
                    <div className="cart__total-price cell" data-title="Location">
                        360 EGP
                    </div>
                   
                </div>
                {/*  End map */}
                
            </div>
            </div>
        </div>
        </div>

        
<div className="orderrecived__row clearfix">
    <div className="orderrecived__subtotal">
        <Col md={3}>
        <h3> Order Sub-Total :</h3>
        <p> Shipping & Handling : </p>

        </Col>

         <Col md={3}>
        <h3> 1440 EGP</h3>
        <p> FREE </p>

        </Col>

  <Col md={6}>

          <h1> 1440  <br/><span class="orderrecived-curnncy">EGP</span> </h1>

        <h2>   Total <br/> Order </h2>


        </Col>
        </div>
</div>

<div className="order-customer">
<h3 className="order-customer__title">Customer Details </h3>

<Row>
    <Col md={10} mdOffset={1}>
<Col md={4}>
<div className="customer__details-name">
<h4> Name : Mahmoud Abdallah  </h4>
</div>
</Col>

<Col md={4}>

<div className="customer__details-email">
<h4>Email : Mahmoud@data.com </h4>
</div>
</Col>


<Col md={4}>
<div className="customer__details-phone">

<h4> Phone Number : (+2) 0123 456 78910 </h4>
</div>
</Col>



<Col md={6}>
<div className="customer__details-Billing ">

<h3> Billing Address :<br/>
 9 Hafez Ramadan st. off Ahmed Fakhry St. Nasr City - Cairo - Egypt. </h3>
</div>

</Col>

<Col md={6}>
<div className="customer__details-shipping">

<h3>Shipping Address :<br/>
 9 Hafez Ramadan st. off Ahmed Fakhry St. Nasr City - Cairo - Egypt. </h3>
</div>

</Col>

<div className="orderrevived__btn">

  <a href="/shop"> <Button  className="orderrecived_back first-btn"> <span><i class="material-icons">
arrow_back
</i>Back To Shopping</span> </Button> </a>



         <a href="#"> <Button  className="orderrecived_print first-btn"> <span><i class="material-icons">
print
</i> Print</span> </Button> </a>

</div>
</Col>
</Row>

</div>
<Features {...this.props}/>
</Grid>
</div>




);
}
}
export default OrderRecived;