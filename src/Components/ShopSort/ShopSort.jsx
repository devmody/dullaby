import React from "react";
import { Row, Col ,MenuItem ,DropdownButton ,ButtonToolbar,Button,Grid,ToggleButtonGroup,ToggleButton } from 'react-bootstrap'

class ShopSort extends React.Component {
render() {
return (
<div className="shopsort products">
        
                <DropdownButton
                    bsSize="large"
                    title={this.props.strings.sortby}
                    id="dropdown-size-large"
                    >
                    <MenuItem eventKey="1">{this.props.strings.action}</MenuItem>
                    <MenuItem eventKey="2">{this.props.strings.antaction}</MenuItem>
                    <MenuItem eventKey="3">{this.props.strings.elsehere}</MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4">{this.props.strings.Separated}</MenuItem>
                </DropdownButton>
                <DropdownButton
                    bsSize="large"
                    title={this.props.strings.itempage}
                    id="dropdown-size-large"
                    >
                    <MenuItem eventKey="1">{this.props.strings.action}</MenuItem>
                    <MenuItem eventKey="2">{this.props.strings.antaction}</MenuItem>
                    <MenuItem eventKey="3">{this.props.strings.elsehere}</MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4">{this.props.strings.Separated}</MenuItem>
                </DropdownButton>
<div className="products_switch">


         <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
      <ToggleButton value={1} onClick={()=>{this.props.settingView('grid')}} ><i class="material-icons" >
view_module
</i></ToggleButton>


      <ToggleButton value={2} onClick={()=>{this.props.settingView('list')}}><i class="material-icons">
view_list
</i></ToggleButton>
    </ToggleButtonGroup>
    </div>



    <div>
    </div>

</div>
);
}
}
export default ShopSort;