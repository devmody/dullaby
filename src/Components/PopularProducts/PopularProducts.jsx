import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import ScrollAnimation from 'react-animate-on-scroll';
class PopularProducts extends React.Component {

render() {
    
  return (


      <div className="popular-products finalsale Newarrive">
     <ScrollAnimation animateIn="fadeInDown"
               			animateOnce={true}>
                     
          <Grid>
          <div className="Newarrive-holder">

              <div className="Newarrive-left">
              <Col md={6}>
              <h4>{this.props.strings.poppro} </h4>
              <p> {this.props.strings.saleext} </p> 
              <span> {this.props.strings.outfits}</span><span>  ... </span> 
              </Col>
              </div>
              <div className="Newarrive-right">

              <Col md={6}>
              <h4> </h4>

              <h4>{this.props.strings.dismore} </h4>
              </Col>
              </div>

              </div>


                <Carousel 
              slidesToShow={3}
      renderCenterLeftControls={({ previousSlide }) => (
        <button onClick={previousSlide}><i class="fa fa-angle-left"></i></button>
      )}
      renderCenterRightControls={({ nextSlide }) => (
        <button onClick={nextSlide}><i class="fa fa-angle-right"></i>

        </button>
      )}>
  
  
              <div className="finalsale__item">
              <a href="/wishlist"><div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish}  </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div> </a>
              <div className="finalsale__item-main">

              <img src={require("../../img/Home/tshirt2new.png")}/>
              </div>
                <div className="finalsale__item-hover">
                <i class="material-icons">
favorite_border
</i>                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                <i class="material-icons"> search </i>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3> {this.props.strings.fitname}  </h3></a>
                  {/* <span className="finalsale__was">2000.00 EGP </span> */}
                  <span className="finalsale__become">{this.props.strings.priceprod}</span>
                  <span className="popular-product__brand">
                  <img src={require("../../img/Home/adidas.png")}/>
                  </span>
                </div>

              </div>

              

     
      
     <div className="finalsale__item">
              <a href="/wishlist"><div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish}  </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div> </a>
              <div className="finalsale__item-main">

              <img src={require("../../img/Home/tshirt2new.png")}/>
              </div>
                <div className="finalsale__item-hover">
                <i class="material-icons">
favorite_border
</i>                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                <i class="material-icons"> search </i>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3> {this.props.strings.fitname}  </h3></a>
                  {/* <span className="finalsale__was">2000.00 EGP </span> */}
                  <span className="finalsale__become">{this.props.strings.priceprod}</span>
                  <span className="popular-product__brand">
                  <img src={require("../../img/Home/adidas.png")}/>
                  </span>
                </div>

              </div>
              

              <div className="finalsale__item">
              <a href="/wishlist"><div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish}  </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div> </a>
              <div className="finalsale__item-main">

              <img src={require("../../img/Home/tshirt2new.png")}/>
              </div>
                <div className="finalsale__item-hover">
                <i class="material-icons">
favorite_border
</i>                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                <i class="material-icons"> search </i>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3> {this.props.strings.fitname}  </h3></a>
                  {/* <span className="finalsale__was">2000.00 EGP </span> */}
                  <span className="finalsale__become">{this.props.strings.priceprod}</span>
                  <span className="popular-product__brand">
                  <img src={require("../../img/Home/adidas.png")}/>
                  </span>
                </div>

              </div>

                 <div className="finalsale__item">
              <a href="/wishlist"><div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish}  </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div> </a>
              <div className="finalsale__item-main">

              <img src={require("../../img/Home/tshirt2new.png")}/>
              </div>
                <div className="finalsale__item-hover">
                <i class="material-icons">
favorite_border
</i>                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                <i class="material-icons"> search </i>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3> {this.props.strings.fitname}  </h3></a>
                  {/* <span className="finalsale__was">2000.00 EGP </span> */}
                  <span className="finalsale__become">{this.props.strings.priceprod}</span>
                  <span className="popular-product__brand">
                  <img src={require("../../img/Home/adidas.png")}/>
                  </span>
                </div>

              </div>
              
      </Carousel>  



       
      
    


      </Grid>


     </ScrollAnimation>
  </div>

      );
  }
}

export default PopularProducts;
