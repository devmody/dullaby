import React from "react";
import { Row, Col } from 'react-bootstrap'
import Carousel from 'nuka-carousel';

class Event extends React.Component {
	
	render() {
                var settings = {
                        dots: true,
                        infinite: true,
                        speed:1000,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        slidesPerRow:1,
                        vertical:{},
                        autoplay:true
                      };
		return (



        <div className="Event">
        
        <div className="container">

     <Carousel wrapAround
      
      renderCenterLeftControls={({ previousSlide }) => (
        <button onClick={previousSlide}><i class="fa fa-angle-left"></i></button>
      )}
      renderCenterRightControls={({ nextSlide }) => (
        <button onClick={nextSlide}><i class="fa fa-angle-right"></i>

        </button>
      )}>
     <div className="newlook">
             <h3 className="event-name newlook-caption">{this.props.strings.event}</h3>
             <p>{this.props.strings.egyfash}</p>
             <p class="event-caption-date">{this.props.strings.date}</p>
     <img src={require("../../img/Home/event.png")}/>
     </div>
     <div className="newlook">
     <h3 className="event-name newlook-caption">{this.props.strings.event}</h3>
             <p>{this.props.strings.egyfash}</p>
             <p class="event-caption-date">{this.props.strings.date}</p>
     <img src={require("../../img/Home/event.png")}/>
     </div>
     

      </Carousel>   

    

      </div>
        </div>

       );
    }
  }
  
  export default Event;
  