import React from "react";
import { ListGroupItem, ListGroup, Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Button } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import UserSetting from "../UserSetting/UserSetting";
class PaymentMethod extends React.Component {
render() {
return (
<Grid>




    <div className="payment__method">
        <Row>
            <UserSetting />
            <Col md={9}>
            <div className="payment__method-bg">

            <div>
                <div className="Payment__methods__title">
                    <h3> Payment Methods </h3>
                </div>
             
                <div className="payment__method-add__holder ">
                    <a href="/"><button type="button" class="payment__method-add btn">  Add New Method</button></a>
                </div>
                <hr className="Payment__methods-hr"/>
            </div>

              <div className="list-group">
                <a href="#" className="list-group-item">
                    <div className="media">
                        <span className="payment__method-delete label  pull-right"><i class="material-icons">delete_forever</i></span>
                        <span className="payment__method-active label pull-right">Active</span>
                        <span className="payment__method-edit label pull-right">Edit</span>
                        <div className="checkbox pull-left">
                        <label class="control control--radio">
      <input   type="radio" name="radio"/>
      <div class="control__indicator"></div>
    </label>
                        </div>
                        <div className="pull-left">
                        </div>
                        <div className="media-body">
                            <h4 className="payment__method-name media-heading">Credit Card ( 1 )</h4>
                            <img className="media-object" src={require("../../img/payment/card.png")} />  
                        </div>
                    </div>
                </a>
            </div>



            <div className="list-group">
            <a href="#" className="list-group-item">
                    <div className="media">
                        <span className="payment__method-delete label  pull-right"><i class="material-icons">delete_forever</i></span>
                        <span className="payment__method-edit label pull-right">Edit</span>
                        <div className="checkbox pull-left">
                        <label class="control control--radio">
      <input  type="radio" name="radio"/>
      <div class="control__indicator"></div>
    </label>
                        </div>
                        <div className="pull-left">
                        </div>
                        <div className="media-body">
                            <h4 className="payment__method-name media-heading">Credit Card ( 1 )</h4>
                            <img className="media-object" src={require("../../img/payment/card.png")} />  
                        </div>
                    </div>
                </a>
            </div>




             <div className="list-group">
            <a href="#" className="list-group-item">
                    <div className="media">
                        <span className="payment__method-delete label  pull-right"><i class="material-icons">delete_forever</i></span>
                        <span className="payment__method-edit label pull-right">Edit</span>
                        <div className="checkbox pull-left">
                        <label class="control control--radio">
      <input   type="radio" name="radio"/>
      <div class="control__indicator"></div>
    </label>
                        </div>
                        <div className="pull-left">
                        </div>
                        <div className="media-body payment__method-paypal">
                            <h4 className="payment__method-name  media-heading">Paypal Account</h4>
                            <img className="media-object" src={require("../../img/payment/paypal.png")} />  
                        </div>
                    </div>
                </a>
            </div>



             <div className="list-group">
            <a href="#" className="list-group-item">
                    <div className="media">
                        <span className="payment__method-delete label  pull-right"><i class="material-icons">delete_forever</i></span>
                        <span className="payment__method-edit label pull-right">Edit</span>
                        <div className="checkbox pull-left">
                        <label class="control control--radio">
      <input  type="radio" name="radio"/>
      <div class="control__indicator"></div>
    </label>
                        </div>
                        <div className="pull-left">
                        </div>
                        <div className="media-body">
                            <h4 className="payment__method-name media-heading">Cash On Delivery Method</h4>
                        </div>
                    </div>
                </a>
            </div>

            <Col md={6}>
            </Col>   
            </div>   
            </Col>
        </Row>
        <hr/>
    </div>
    <Features/>
</Grid>

);
}
}
export default PaymentMethod;