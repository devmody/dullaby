import React from "react";
import { Row, Col, MenuItem, Nav, Navbar, NavItem, NavDropdown, Button, Panel, Grid, DropdownButton, FormGroup, FormControl } from 'react-bootstrap'
import InputNumber from 'rc-input-number';
import 'rc-input-number/assets/index.css';
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Features from "../Features/Features";
import Quantity from "../Quantity/Quantity";

class Headerbottom extends React.Component {
render() {
return (
<div className="cart">
    <Breadcrumbing {...this.props} />
    <Grid>
        <div className="cart-bg">
            <div className="table">
                <div className="row main header">
                    <div className="cell product">
                    {this.props.strings.prod} 
                    </div>
                    <div className="cell Price">
                    {this.props.strings.price} 
                    </div>
                    <div className="cell Quantity">
                    {this.props.strings.quantity}                     
                    </div>
                    <div className="cell Total-Price">
                    {this.props.strings.totalpric}  
                    </div>
                    <div className="cell Action">
                    {this.props.strings.action} 
                        
                    </div>
                </div>
                {/*  You can make map from here */}
                <div className="row cart__item-one">
                    <div className="cell" data-title="Name">
                        <div className="cart-img col-md-3">
                            <span> <img src={require("../../img/cart/shirt.png")} />  </span>
                        </div>
                        <div className="cart-name col-md-9">
                            <a href="#">{this.props.strings.teeshirt} </a>
                            <div className="cart-category">
                                <p>{this.props.strings.categ}<span class="cart__category-type">{this.props.strings.menshirts}  </span></p>
                                <p>{this.props.strings.colorr} <span class="cart__colour">{this.props.strings.blue}  </span></p>
                                <p>{this.props.strings.sizee}<span class="cart__size"> XXl  </span></p>
                            </div>
                        </div>
                    </div>
                    <div className="cart__price cell" data-title="Age">
                    {this.props.strings.salary}  
                    </div>
                    <Quantity/>
                    <div className="cart__total-price cell" data-title="Location">
                    {this.props.strings.salary} 
                    </div>
                    <div className="cart__action-control cell" data-title="">
                        <span className="cart__favorite">
                        <i class="material-icons">
                        favorite_border
                        </i>
                        </span>
                        <span className="cart__delete">
                        <i class="material-icons">
                        delete_forever
                        </i>
                        </span>
                    </div>
                </div>
               
                {/*  End map */}

                  {/*  You can make map from here */}
                  <div className="row cart__item-one">
                    <div className="cell" data-title="Name">
                        <div className="cart-img col-md-3">
                            <span> <img src={require("../../img/cart/shirt.png")} />  </span>
                        </div>
                        <div className="cart-name col-md-9">
                            <a href="#">{this.props.strings.teeshirt} </a>
                            <div className="cart-category">
                                <p>{this.props.strings.categ}<span class="cart__category-type">{this.props.strings.menshirts}  </span></p>
                                <p>{this.props.strings.colorr} <span class="cart__colour">{this.props.strings.blue}  </span></p>
                                <p>{this.props.strings.sizee}<span class="cart__size"> XXl  </span></p>
                            </div>
                        </div>
                    </div>
                    <div className="cart__price cell" data-title="Age">
                    {this.props.strings.salary}  
                    </div>
                    <Quantity/>
                    <div className="cart__total-price cell" data-title="Location">
                    {this.props.strings.salary} 
                    </div>
                    <div className="cart__action-control cell" data-title="">
                        <span className="cart__favorite">
                        <i class="material-icons">
                        favorite_border
                        </i>
                        </span>
                        <span className="cart__delete">
                        <i class="material-icons">
                        delete_forever
                        </i>
                        </span>
                    </div>
                </div>
               
                {/*  End map */}
                
            </div>
        </div>
        <Col md={4}>
        <div className="cart__calucate-shipping">
            <h3 className="cart__calucate-shipping-name name-blue">{this.props.strings.calcu}  </h3>
            <Row>
                <Col md={12}>
                <DropdownButton title={this.props.strings.country} id="country">
                    <MenuItem eventKey="1">{this.props.strings.action}</MenuItem>
                    <MenuItem eventKey="2">{this.props.strings.anotaction}</MenuItem>
                    <MenuItem eventKey="3">{this.props.strings.someelse}</MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4">{this.props.strings.separated}</MenuItem>
                </DropdownButton>
                </Col>
                <Col md={6}>
                <DropdownButton title={this.props.strings.city} id="citystate">
                <MenuItem eventKey="1">{this.props.strings.action}</MenuItem>
                    <MenuItem eventKey="2">{this.props.strings.anotaction}</MenuItem>
                    <MenuItem eventKey="3">{this.props.strings.someelse}</MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4">{this.props.strings.separated}</MenuItem>
                </DropdownButton>
                </Col>
                <Col md={6}>
                <FormGroup controlId="formHorizontalEmail" className="zip-code">
                    <FormControl type="text" placeholder="ZIP Code *" />
                </FormGroup>
                </Col>
                <div className="cart_update">
                    <a href="/"><button type="button" class="cart_update btn  btn-blue">{this.props.strings.update}  </button></a>
                </div>
            </Row>
        </div>
        </Col>
        <Col md={4}>
        <div className="cart__calucate-shipping cart-holder">
            <h3 className="cart__calucate-shipping-name name-blue">{this.props.strings.coupon}</h3>
            <p className="cart__calucate-shipping-caption">{this.props.strings.loreem}</p>
            <Row>
                <Col md={12}>
                <FormGroup controlId="formHorizontalEmail" className="addcode">
                    <FormControl type="text" placeholder={this.props.strings.codee} />
                </FormGroup>
                </Col>
                <div className="cart_update">
                    <a href="/"><button type="button" class="cart-apply_btn btn  btn-blue">{this.props.strings.app}</button></a>
                </div>
            </Row>
        </div>
        </Col>
        <Col md={4}>
        <div className="Cart-total">
            <h3>{this.props.strings.cartt} </h3>
            <Row>
                <Col md={8}>
                <p className="Cart__total-left">{this.props.strings.sub} </p>
                </Col>
                <Col md={4}>
                <p className="Cart__total-right">{this.props.strings.salary}</p>
                </Col>
                <Col md={8}>
                <p className="Cart__total-left">{this.props.strings.handle}</p>
                </Col>
                <Col md={4}>
                <p className="Cart__total-right">{this.props.strings.free} </p>
                </Col>
                <div className="clearfix">
                    <Col md={8}>
                    <p className="Cart__total-left">{this.props.strings.total} </p>
                    </Col>
                    <Col md={4}>
                    <p className="Cart__total-right">{this.props.strings.salary}</p>
                    </Col>
                </div>
                <div className="cart_proceed">
                    <a href="/checkout"><button type="button" class="cart_proceed_btn btn ">{this.props.strings.checkout}<i class="fa fa-arrow-right"></i></button></a>
                </div>
            </Row>
        </div>
        </Col>
    </Grid>
    <Features {...this.props}/>

</div>
);
}
}
export default Headerbottom;