import React from "react";
import { Row, Col ,Grid ,ToggleButton,ButtonToolbar,ToggleButtonGroup,Button,FormGroup,FormControl} from 'react-bootstrap'
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import Quantity from "../Quantity/Quantity"
import { Carousel } from 'react-responsive-carousel';
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Paginations from "../Paginations/Paginations";

class Reviews extends React.Component {
    
render() {
return (
    <div className="reviews">
  <div className="Reviews-holder">

<div className="reviews_form">
<Row>
<Col md={8}>
 <div className="Reviews__left" >
 <span className="Review__conecct">Connect with: </span>
 <span className="Review__social">
                <a href="#">
                <i className="fa fa-facebook"></i> </a>
        
                <a href="#">
                <i className="fa fa-twitter"></i> </a>
          
            
         </span>
</div>
 </Col>

   <Col md={4}>



  <div className="Reviews__right">

  <div className="Reviews__rating your-rate">
                        
                              <ul>
                                  <li><a href="#"><i className="fa fa-star" /></a></li>
                                  <li><a href="#"><i className="fa fa-star" /></a></li>
                                  <li><a href="#"><i className="fa fa-star" /></a></li>
                                  <li><a href="#"><i className="fa fa-star" /></a></li>
                                  <li><a href="#"><i className="fa fa-star" /></a></li>
                              </ul>
                         

</div>

  <div className="reviews__your-rate">
  <span className="reviews__your-rate">Your Rate: </span>
</div>



                     

 </div>

     </Col>



   </Row>


 <FormGroup bsSize="large">
    <FormControl type="text" placeholder="First Name *" />
    <FormControl type="text" placeholder="Your E-Mail *" />
    <FormGroup controlId="formControlsTextarea">
      <FormControl componentClass="textarea" placeholder="Your Review *" />
    </FormGroup>
    
  </FormGroup> 
  </div>
  <a href="#">  <Button className="reviews__btn-submit  first-btn">Submit Your Review</Button> </a> 
  
<div className="hello">
<Row>
<Col md={2}>
   <div className="reviews__person-img">

    <img src={require("../../img/review/person.png")} /> 
</div>
   </Col>

   <Col md={10}>

  <Col md={6}>
   <div className="Reviews__left" >
   <span className="Review__name">Mahmoud </span>
   <span className="Review__date">Aug 11, 2018 </span>
</div>
   </Col>
   <Col md={4}>
    <div className="Reviews__right">
    <div className="Reviews__rating">
                          
                                <ul>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                </ul>
                           

 </div>


           {/* <Reviews/>           */}

   </div>
   </Col>

         <Col md={2}>

       <div>   <a href="#"><Button className="reviews__btn-reply  first-btn">Reply</Button>  </a>  </div>
       </Col>




   <div className="Reviews__comment">
   <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsdsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sddit aspernatur aut odit aut fugit .</p>
   </div>

   </Col>
   </Row>

  </div>

  <div className="hello">
<Row>
<Col md={2}>
   <div className="reviews__person-img">

    <img src={require("../../img/review/person.png")} /> 
</div>
   </Col>

   <Col md={10}>

  <Col md={6}>
   <div className="Reviews__left" >
   <span className="Review__name">Mahmoud </span>
   <span className="Review__date">Aug 11, 2018 </span>
</div>
   </Col>
   <Col md={4}>
    <div className="Reviews__right">
    <div className="Reviews__rating">
                          
                                <ul>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                </ul>
                           

 </div>


           {/* <Reviews/>           */}

   </div>
   </Col>

         <Col md={2}>

       <div>   <a href="#"><Button className="reviews__btn-reply  first-btn">Reply</Button>  </a>  </div>
       </Col>




   <div className="Reviews__comment">
   <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsdsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sddit aspernatur aut odit aut fugit .</p>
   </div>

   </Col>
   </Row>
   <Paginations/>

  </div>
  
  </div>
     </div>


);
}
}
export default  Reviews;