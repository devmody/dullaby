import React from "react";
import { Row, Col } from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import ScrollAnimation from 'react-animate-on-scroll';

class Category extends React.Component {
        constructor(props){
                super(props)
        }
        render() {
                return (
                        <div className="category">
                                <div className="category-header">
                                        <h3>
                                                {this.props.strings.Yours}
                                        </h3>
                                        <p>
                                        {this.props.strings.Choose}
                                
                                                 </p>
                                </div>
                                <div className="container">
                                        <Row>
                                                <ScrollAnimation animateIn="fadeIn"
                                                >
                                                        <Col md={2} Col xs={6} className="custom-width">
                                                                <div className="category-item">
                                                                        <div className="category-item-holder">

                                                                                <a href="/shop"> <img src={require("../../img/Home/jeans.png")} /> </a>
                                                                        </div>
                                                                        <a href="/shop"> <h3> 
                                                                        {this.props.strings.Jeans}
                                                                                </h3> </a>
                                                                </div>
                                                        </Col>
                                                </ScrollAnimation>

                                                <ScrollAnimation animateIn="fadeIn"
                                                >

                                                        <Col md={2} Col xs={6} className="custom-width">
                                                                <div className="category-item">
                                                                        <div className="category-item-holder">

                                                                                <a href="/shop"> <img src={require("../../img/Home/tshirt.png")} /> </a>
                                                                        </div>
                                                                        <a href="/shop">  <h3>
                                                                        {this.props.strings.Shirts}
                                                                         </h3></a>
                                                                </div>
                                                        </Col>
                                                </ScrollAnimation>

                                                <ScrollAnimation animateIn="fadeIn"
                                                >

                                                        <Col md={2} Col xs={6} className="custom-width">
                                                                <div className="category-item">
                                                                        <div className="category-item-holder">

                                                                                <a href="/shop">  <img src={require("../../img/Home/shose.png")} /></a>
                                                                        </div>
                                                                        <a href="/shop"> <h3> {this.props.strings.Shoes} </h3> </a>
                                                                </div>
                                                        </Col>
                                                </ScrollAnimation>

                                                <ScrollAnimation animateIn="fadeIn"
                                                >

                                                        <Col md={2} Col xs={6} className="custom-width">
                                                                <div className="category-item">
                                                                        <div className="category-item-holder">

                                                                                <a href="/shop">  <img src={require("../../img/Home/perfum.png")} /></a>
                                                                        </div>
                                                                        <a href="/shop"> <h3> {this.props.strings.Perfumes}
                                                                                </h3> </a>
                                                                </div>
                                                        </Col>
                                                </ScrollAnimation>

                                                <ScrollAnimation animateIn="fadeIn"
                                                >

                                                        <Col md={2} Col xs={6} className="custom-width">
                                                                <div className="category-item">
                                                                        <div className="category-item-holder">

                                                                                <a href="/shop"> <img src={require("../../img/Home/short.png")} /></a>
                                                                        </div>
                                                                        <a href="/shop"> <h3>
                                                                        {this.props.strings.Shorts}
                                                                         </h3> </a>
                                                                </div>
                                                        </Col>
                                                </ScrollAnimation>


                                        </Row>

                                        <Carousel>
                                                <div className="newlook">
                                                        <div className="newlook__share share">
                                                                <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>

                                                        </div>

                                                        <h3 className="newlook-caption"> {this.props.strings.New}</h3>
                                                        <p>{this.props.strings.Show}</p>
                                                        <img src={require("../../img/Home/newlook.png")} />
                                                </div>
                                                <div className="newlook">
                                                        <h3 className="newlook-caption"> {this.props.strings.New}</h3>
                                                        <p>{this.props.strings.Show}</p>
                                                        <img src={require("../../img/Home/newlook.png")} />
                                                </div>


                                        </Carousel>



                                </div>
                        </div>

                );
        }
}

export default Category;
