import React from "react";
import { Row, Col, MenuItem, Nav, Navbar, NavItem, NavDropdown, Button, PanelToggleButton, ToggleButtonGroup, ButtonToolbar, ToggleButton, DropdownButton } from 'react-bootstrap'
import { Link } from 'react-router-dom'

class Headerbottom extends React.Component {
  // constructor(props, context) {
  //   super(props, context);

  //   this.state = {
  //     open: false
  //   };


  // }


  constructor() {
    super();
  }
  componentDidMount() {
    if (sessionStorage.getItem("lang") === "en") {
      document.getElementById("englishBtn").click()
    } else {
      document.getElementById("arabicBtn").click()
    }
  }

  render() {
    return (
      <div className="headerbottom">
        <Navbar collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/"> <img src={require("../../img/Home/home.png")} /></a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav className="headerbottom-menu">

              <NavItem eventKey={1} href="/T-Shirts">


              </NavItem>

              <NavDropdown eventKey="4" title= {this.props.strings.Jeans}  id="nav-dropdown">
              
                <Col md={7}>
                  <div className="left-menu">

                    <MenuItem eventKey="4.1" href="/shop">
                      <a href="/"><img className="headerbottom-img" src={require("../../img/Home/jeans-menu.png")} />  <Link to="/shop">Slim fit Jeans  </Link>   </a>

                    </MenuItem>
                    <MenuItem divider />

                    <MenuItem eventKey="4.2" href="/shop">
                      <a href="/"><img className="headerbottom-img" src={require("../../img/Home/jeans-menu.png")} />  <Link to="/shop">Slim fit Jeans  </Link>   </a>

                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4.3" href="/shop">
                      <a href="/"><img className="headerbottom-img" src={require("../../img/Home/jeans-menu.png")} />  <Link to="/shop">Slim fit Jeans  </Link>   </a>

                    </MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey="4.4" href="/shop">
                      <a href=""><img className="headerbottom-img" src={require("../../img/Home/jeans-menu.png")} />  <Link to="/shop">Slim fit Jeans  </Link>   </a>


                    </MenuItem>
                    <MenuItem divider />



                    <MenuItem eventKey="4.6" href="/shop">
                      <img className="headerbottom-img" src={require("../../img/Home/jeans-menu.png")} />   <Link to="/shop">Slim fit Jeans  </Link>
                    </MenuItem>
                    <MenuItem divider />

                    <MenuItem eventKey="4.7" href="/shop">
                      <img className="headerbottom-img" src={require("../../img/Home/jeans-menu.png")} />  <Link to="/shop">Slim fit Jeans  </Link>

                    </MenuItem>





                  </div>
                </Col>
                <Col md={5}>
                  <div className="right-menu">
                    <MenuItem eventKey="4.8">
                      <div className="right-menu__title">
                        <h3> Popular Brands </h3>

                      </div>
                      <Col md={4}>
                        <img className="headerbottom-img" src={require("../../img/Home/addidas.png")} />
                      </Col>
                      <Col md={4}>
                        <img className="headerbottom-img" src={require("../../img/Home/levis.png")} />
                      </Col>
                      <Col md={4}>
                        <img className="headerbottom-img" src={require("../../img/Home/lacost.png")} />
                      </Col>

                      <Col md={4}>
                        <Link to="/shop"> <img className="headerbottom-img" src={require("../../img/Home/polo.png")} /> </Link>
                      </Col>
                      <Col md={4}>
                        <img className="headerbottom-img" src={require("../../img/Home/bsk.png")} />
                      </Col>
                      <Col md={4}>
                        <img className="headerbottom-img" src={require("../../img/Home/pb.png")} />
                      </Col>
                      <div className="headerbottom-Menu__discover">
                        <a href="/shop">  Discover More...</a>
                      </div>
                      <MenuItem divider />

                      <div className="headerbottom-Menu__buttons">

                        <Button className="headerbottom_offers second-btn">Jeans Offers</Button>
                        <Button className="headerbottom_collections first-btn">Jeans Collections</Button>
                      </div>
                    </MenuItem>
                  </div>
                </Col>

              </NavDropdown>


              <NavItem eventKey={3} href="/shop">
                {this.props.strings.shirts}
              </NavItem>

              <NavItem eventKey={4} href="/shoes">
                {this.props.strings.shoes}
              </NavItem>

              <NavItem eventKey={5} href="/Accessories">
                {this.props.strings.Accessories}
              </NavItem>
              <NavItem eventKey={6} href="/Collections">
                {this.props.strings.Collections}
              </NavItem>
              <NavItem eventKey={7} href="/Outfits">
                {this.props.strings.Outfits}
              </NavItem>




              <NavItem eventKey={8} href="/Brands">
                {this.props.strings.Brands}


              </NavItem>


            </Nav>

            <Nav pullRight>
              <div className="eee" eventKey={1} href="/shop">
                <Button className="headertop_offers" bsStyle="primary">
                {this.props.strings.OFFERS}
                </Button>
              </div>




  

             

            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>

    );
  }
}

export default Headerbottom;
