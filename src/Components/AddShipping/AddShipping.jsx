import React from "react";
import { ListGroupItem, ListGroup, Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Button, Tab, NavItem, Nav } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import UserSetting from "../UserSetting/UserSetting";

class AddShipping extends React.Component {
    render() {
        return (
            <Grid>
                <div className="addshipping">
                <div className="payment__empty">
                    <Row>
                        <UserSetting />
                        <div className="text">
                            <Col md={9}>
                                <div className="payment__method-bg">
                                    <div>
                                        <div className="Payment__methods__title">
                                            <h3> Add Shipping Address Information</h3>
                                        </div>




                                        <hr className="Payment__methods-hr" />
                                    </div>
                                    <div className="addshipping__content">


<Form horizontal>

  <FormGroup controlId="formHorizontalEmail">

      <Col componentClass={ControlLabel} sm={2}>
      Location Map :
    </Col>
    <Col sm={10}>
      <FormControl  className="addshipping-firstname"  type="text" placeholder="Add Location Manually" />

    </Col>
    <Col sm={12}>


      <img className="media-object" src={require("../../img/payment/maps.png")} />  

</Col>
    <Col componentClass={ControlLabel} sm={2}>
    First Name * :
    </Col>
    <Col sm={4}>
      <FormControl  className="addshipping-firstname"  type="text" placeholder="Email" />
    </Col>

    <Col componentClass={ControlLabel} sm={2}>
    Last Name * :
    </Col>
    <Col sm={4}>
      <FormControl  className="addshipping-lastname"  type="text" placeholder="Mody" />
    </Col>


 <Col componentClass={ControlLabel} sm={2}>
 Email  * :
    </Col>
    <Col sm={10}>
      <FormControl  className="addshipping-email"  type="text" placeholder="Dullaby@gmail.com" />
    </Col>




     <Col componentClass={ControlLabel} sm={2}>
     Address 1 * :
    </Col>
    <Col sm={10}>
      <FormControl  className="addshipping-address"  type="text" placeholder="Lorem ipsum dolor sit amet," />
    </Col>


   <Col componentClass={ControlLabel} sm={2}>
     Address 2 * :
    </Col>
    <Col sm={10}>
      <FormControl  className="addshipping-address2"  type="text" placeholder="Lorem ipsum dolor sit amet, 2" />
    </Col>


    <Col componentClass={ControlLabel} sm={2}>
    Country  * :

</Col>
<Col sm={10}>

<DropdownButton title="Expire Month" id="addshipping-country">
    <MenuItem eventKey="1">Lorem ipsum ,</MenuItem>
    <MenuItem eventKey="2">Another action</MenuItem>
    <MenuItem eventKey="3">Something else here</MenuItem>
    <MenuItem divider />
    <MenuItem eventKey="4">Separated link</MenuItem>
</DropdownButton>
</Col>




    <Col componentClass={ControlLabel} sm={2}>
    city * :

</Col>

<Col sm={4}> 
<DropdownButton title="Expire Month" id="addshipping-city">
    <MenuItem eventKey="1">Lorem ipsum ,</MenuItem>
    <MenuItem eventKey="2">Another action</MenuItem>
    <MenuItem eventKey="3">Something else here</MenuItem>
    <MenuItem divider />
    <MenuItem eventKey="4">Separated link</MenuItem>
</DropdownButton>
</Col>





<Col componentClass={ControlLabel} sm={2}>
   Town * :

</Col>

<Col sm={4}> 
<DropdownButton title="Expire Month" id="addshipping-town">
    <MenuItem eventKey="1">Lorem ipsum ,</MenuItem>
    <MenuItem eventKey="2">Another action</MenuItem>
    <MenuItem eventKey="3">Something else here</MenuItem>
    <MenuItem divider />
    <MenuItem eventKey="4">Separated link</MenuItem>
</DropdownButton>
</Col>




<Col componentClass={ControlLabel} sm={2}>
Phone * :

</Col>

<Col sm={4}> 
<DropdownButton title="Expire Month" id="addshipping-phone">
    <MenuItem eventKey="1">Lorem ipsum ,</MenuItem>
    <MenuItem eventKey="2">Another action</MenuItem>
    <MenuItem eventKey="3">Something else here</MenuItem>
    <MenuItem divider />
    <MenuItem eventKey="4">Separated link</MenuItem>
</DropdownButton>
</Col>




<Col componentClass={ControlLabel} sm={2}>
Mobile * :

</Col>

<Col sm={4}> 
<DropdownButton title="Expire Month" id="addshipping-mobile">
    <MenuItem eventKey="1">Lorem ipsum ,</MenuItem>
    <MenuItem eventKey="2">Another action</MenuItem>
    <MenuItem eventKey="3">Something else here</MenuItem>
    <MenuItem divider />
    <MenuItem eventKey="4">Separated link</MenuItem>
</DropdownButton>
</Col>




<Col componentClass={ControlLabel} sm={2}>
Notes * :

</Col>

      <Col sm={10}> 

      <FormControl componentClass="textarea" placeholder="" />
</Col>

  </FormGroup>


  </Form>



                                    </div>
                                    <div className="Add-payment__method-buttons">
    <span><a href="#"><button type="button" className="addshipping-add__adress btn ">Add Address</button></a> </span>
       
      <span> <a href=""><button type="button" className="Add-shipping__cancel-process btn">Cancel Process</button></a> </span>
</div>
                                </div>

                            </Col>
                        </div>

                

                    </Row>
                    <hr />
                </div>
                </div>
                <Features />
            </Grid>

        );
    }
}
export default AddShipping;