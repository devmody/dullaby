import React from "react";
import { Row, Col ,Grid ,ToggleButton,ButtonToolbar,ToggleButtonGroup,Button} from 'react-bootstrap'
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import Quantity from "../Quantity/Quantity"
import { Carousel } from 'react-responsive-carousel';
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Reviews from "../Reviews/Reviews";
import PopularProducts from "../PopularProducts/PopularProducts";
import TopRating from "../TopRating/TopRating";
import FeaturesProducts from "../FeaturesProducts/FeaturesProducts";


class SingleProducts extends React.Component {
    
render() {
return (    
<div className="singleproduct">
<Breadcrumbing/>
<Grid>
<div className="singleproduct-bg">
    <Row>
        <Col md={6}>
 <Carousel
                                    autoPlay={true}
                                    swiping={true}
                                    swipeScrollTolerance={10}
                                    showArrows={false}
                                    showStatus={false}
               >
                <div className="single-item">
                <img src={require("../../img/Product/tshirtt.png")}/>     
                </div>
                <div className="single-item">
                <img src={require("../../img/Product/tshirtt.png")}/>                 </div>
                <div className="single-item">
                <img src={require("../../img/Product/tshirtt.png")}/>                 </div>
                <div className="single-item">
                <img src={require("../../img/Product/tshirtt.png")}/>                 </div>
                <div className="single-item">
                <img src={require("../../img/Product/tshirtt.png")}/>                 </div>
                <div className="single-item">
                <img src={require("../../img/Product/tshirtt.png")}/>                 </div>
            </Carousel>

          <ul className="headertop_menu">
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-facebook"></i> </a>
            </li>
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-twitter"></i> </a>
            </li>
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-youtube"></i> </a>
            </li>
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-instagram"></i> </a>
            </li>
        </ul>
        </Col>


        <Col md={6}>
        <div className="signel-discount">
        <h3>Discount 25% </h3>
        </div>
        <div className="singleproduct__title">
       <p> M Shirt Lorem ipsum dolor sit amet, consectetur adipisicing elit </p>
        </div>
        <div className="rating">
                            <span>
                                <ul>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                </ul>
                            </span>
                            <span className="number-review"> (130 review) </span>
                        </div>

                              <div className="singleproduct__description">
       <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in</p>
                        </div>
                        <hr/>

                        <div className="signel-discount">
                        <ul className="menu-price">
                            <li className="new-price">150.00 EGP</li>
                            <li className="was-price">200.00 EGP</li>
                        </ul>
                        <p> Value-added tax INCLUDES </p>
                        </div>
                        <hr/>
<div className="single-colours">
<span>Available  Colours : </span>

<ButtonToolbar>
    <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
      <ToggleButton value={1}></ToggleButton>
      <ToggleButton value={2}></ToggleButton>
      <ToggleButton value={3}></ToggleButton>
    </ToggleButtonGroup>
  </ButtonToolbar>


</div>

<div className="single-sizes">
<span>Available  Sizes : </span>
<ButtonToolbar>
    <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
      <ToggleButton value={1}>M</ToggleButton>
      <ToggleButton value={2}>L</ToggleButton>
      <ToggleButton value={3}>XL</ToggleButton>
      <ToggleButton value={4}>2XL</ToggleButton>
      <ToggleButton value={5}>3XL</ToggleButton>

    </ToggleButtonGroup>
  </ButtonToolbar>


</div>

<div className="single-quantit">
<span>Product Quantity : </span>

<Quantity/>
</div>

<div className="single-buttons">
<span> 
     <a href="/cart"> <Button  className="add-to-cart second-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i>Add to Cart </Button> </a></span>
             
                        <a href="#"> <span className="wishlist-icon"><i class="fa fa-heart" aria-hidden="true"></i> </span></a>
</div>



<hr/>
<div className="single-category">
<span> Product Category :  </span> <span><a href="#"> Men Shirts</a></span>
</div>




        

        </Col>


    </Row>
</div>



<Row>
<Col md={3}>
<TopRating/>
<FeaturesProducts/>
</Col>

<Col md={9}>
<Reviews/>  
</Col>
</Row>

    </Grid>
    <PopularProducts/>

</div>

);
}
}
export default SingleProducts;