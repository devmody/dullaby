import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import Slider from "react-slick";

class NewCollection extends React.Component {
	
	render() {
        var settings = {
            dots: true,
            infinite: true,
            speed:1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            slidesPerRow:1,
            vertical:{},
            autoplay:true
          };


		return (


        <div className="Newcollection">
           <Grid>
           <Row>
           <Slider {...settings}>

        <div className="Newcollection-item_holder">
        <div className="Newcollection-item">

          <h3>{this.props.strings.Formal}</h3>
          <h4>{this.props.strings.Outfit} </h4>
          <p>{this.props.strings.occasion}  </p>


</div>

<div className="Newcollection-item__img">
<a href="#">{this.props.strings.moore} </a>

<img src={require("../../img/Home/collection.png")}/>

</div>
</div>
<div className="Newcollection-item_holder">
        <div className="Newcollection-item">

      <h3>{this.props.strings.Formal}</h3>
          <h4>{this.props.strings.Outfit} </h4>
          <p>{this.props.strings.occasion}  </p>


</div>

<div className="Newcollection-item__img">

<img src={require("../../img/Home/collection2.png")}/>
<a href="#"> {this.props.strings.moore} </a>

</div>
</div>
        
       
        
        
      </Slider>
        </Row>

        </Grid>

  

    </div>

       );
    }
  }
  
  export default NewCollection;
  