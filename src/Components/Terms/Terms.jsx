import React from "react";

import { Grid, Row } from 'react-bootstrap'
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";

class Terms extends React.Component {
  render() {
    return (
  
        <Grid>
            <Breadcrumbing {...this.props}/>

        <div className="terms">
            
        <div className="terms__content">
            <Row>
              <div className="terms__content-condittion">
                <h3>{this.props.strings.ecom}</h3>
                <p>
                {this.props.strings.loreem}
    
                </p>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="terms__list-number">
                    <h3>{this.props.strings.number} 
                    </h3>
                    <ol>
                      <li><span>{this.props.strings.consectetur} </span></li>
                      <li> <span>{this.props.strings.eiusmod} </span></li>
                      <li> <span>{this.props.strings.tempor}  </span></li>
                    </ol>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="terms__list-markered">
                    <h3>{this.props.strings.mark} </h3>
                    <ul>
                    <li><span>{this.props.strings.consectetur} </span></li>
                      <li> <span>{this.props.strings.eiusmod} </span></li>
                      <li> <span>{this.props.strings.tempor}  </span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="clearfix" />
              {/* TABLE */}
              <div className="table-action__title">
                <h3>{this.props.strings.table} </h3>
              </div><table className="table table-action">
                <thead>
                  <tr>
                    <th className="t-smallpopular">{this.props.strings.popitem}</th>
                    <th className="t-smallsize">{this.props.strings.size}</th>
                    <th className="t-smallcolor">{this.props.strings.color}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="popular-item">{this.props.strings.drinks}</td>
                    <td>{this.props.strings.inche} </td>
                    <td>{this.props.strings.color}</td>
                  </tr>
                  <tr>
                    <td className="popular-item">{this.props.strings.grow} </td>
                    <td>{this.props.strings.inche} </td>
                    <td>{this.props.strings.color}</td>
                  </tr>
                  <tr>
                    <td className="popular-item">{this.props.strings.grow}  </td>
                    <td>{this.props.strings.inche} </td>
                    <td>{this.props.strings.color}</td>
                  </tr>
                  <tr>
                    <td className="popular-item">{this.props.strings.grow}</td>
                    <td>{this.props.strings.inche}</td>
                    <td>{this.props.strings.color}</td>
                  </tr>
                </tbody>
              </table>
              <div className="terms-quote">
                <h3>{this.props.strings.text}</h3>
                <p> {this.props.strings.loreim} <em className="terms__special-text">{this.props.strings.emone}  </em>{this.props.strings.emtwo}  </p>
                <h4>
                {this.props.strings.loreimm}
                          
                </h4>
                <p className="terms-quote__second">
                {this.props.strings.loreim}
                <em className="terms-quote__second-custome">{this.props.strings.emone} </em> {this.props.strings.emtwo} 
                </p>
              </div>
             
              {/* END TABLE */}
            </Row>
          </div>
        </div>
      </Grid>
       );
    }
  }
  
  export default Terms;
  