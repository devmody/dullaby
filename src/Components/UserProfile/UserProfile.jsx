import React from "react";
import { Row, Col, Grid, PageHeader, Button, FormControl, ControlLabel, MenuItem, DropdownButton, InputGroup, FormGroup, Form } from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import UserSetting from "../UserSetting/UserSetting";
import DropzoneS3Uploader from 'react-dropzone-s3-uploader'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';


class UserProfile extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      startDate: moment()
    };
    this.handleChange = this.handleChange.bind(this);
    this.state = { country: '', region: '' };

  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }



  selectCountry(val) {
    this.setState({ country: val });
  }

  selectRegion(val) {
    this.setState({ region: val });
  }

  render() {
    const { country, region } = this.state;

    const uploadOptions = {
      server: 'http://localhost:4000',
      signingUrlQueryParams: { uploadType: 'avatar' },
    }


    const s3Url = 'https://my-bucket.s3.amazonaws.com'

    return (

      <Grid>
        <div class="user-profile">

          <div className="Address-empty order-empty payment__empty">
            <Row>

              <UserSetting {...this.props} />
              <div className="text">
                <Col md={9}>
                  <div className="payment__method-bg">
                    <div>
                      <div className="Payment__methods__title">
                        <h3>{this.props.strings.basiinfo}</h3>
                      </div>

                      <hr className="Payment__methods-hr" />
                      <Row>

                        <Col md={8} mdOffset={2}>
                          <div className="order-history__content">
                            <div className="user-profile upload">
                              <img src={require("../../img/user/profile.png")} />
                              <h3>{this.props.strings.clicuplod} </h3>
                              <DropzoneS3Uploader

                                onFinish={this.handleFinishedUpload}
                                s3Url={s3Url}
                                maxSize={1024 * 1024 * 5}
                                upload={uploadOptions}
                              />
                            </div>
                            <Row>
                              <div className="user-profile__content clearfix">
                                <Col componentClass={ControlLabel} sm={2}>
                                  {this.props.strings.fulname}
                                </Col>
                                <Col sm={10}>
                                  <FormControl className="custom-hight" type="text" placeholder={this.props.strings.mahmod} />
                                </Col>
                              </div>


                              <div className="user-profile__content clearfix">

                                <Col componentClass={ControlLabel} sm={2}>
                                  {this.props.strings.emil}
                                </Col>
                                <Col sm={10}>
                                  <FormControl className="custom-hight" type="text" placeholder="Mahmoud@mahmoud.com" />
                                </Col>
                              </div>
                              <div className="user-profile__content clearfix">


                                <Col componentClass={ControlLabel} sm={2}>
                                  {this.props.strings.gender}

                                </Col>

                                <Col sm={10}>
                                  <DropdownButton title={this.props.strings.male} id="user-profile__gender">
                                    <MenuItem eventKey="1">{this.props.strings.male}</MenuItem>
                                    <MenuItem eventKey="2">{this.props.strings.female}</MenuItem>
                                    <MenuItem divider />
                                    <MenuItem eventKey="3">{this.props.strings.other}</MenuItem>
                                  </DropdownButton>
                                </Col>


                              </div>

                              <hr />
                              <div className="user-profile__birthday">
                                <h3>{this.props.strings.birth} </h3>
                                <DatePicker placeholderText={this.props.strings.dateselec}
                                  selected={this.state.startDate}
                                  onChange={this.handleChange}
                                  isClearable={true}

                                  peekNextMonth
                                  showMonthDropdown
                                  showYearDropdown
                                  dropdownMode="select"

                                />
                              </div>
                              <hr />

                              <div className="user-profile__region clearfix">

                                <Col componentClass={ControlLabel} sm={2}>
                                  {this.props.strings.country}
                                </Col>
                                <Col sm={4}>
                                  <CountryDropdown
                                    value={country}
                                    onChange={(val) => this.selectCountry(val)} />    </Col>

                                <Col componentClass={ControlLabel} sm={2}>
                                  {this.props.strings.state}
                                </Col>
                                <Col sm={4}>

                                  <RegionDropdown
                                    country={country}
                                    value={region}
                                    onChange={(val) => this.selectRegion(val)} />
                                </Col>
                                

                              </div>

                              <div className="user-profile__info">


                                <div className="user-profile__info-item">

                                  <Col componentClass={ControlLabel} sm={2}>
                                    {this.props.strings.postal}


                                  </Col>

                                  <Col sm={4}>
                                    <FormControl className="user-profile__postal custome-hight" type="text" placeholder="12345" />

                                  </Col>



                                  <div className="user-profile__info-item">

                                    <Col componentClass={ControlLabel} sm={2}>
                                      {this.props.strings.phone}


                                    </Col>

                                    <Col sm={4}>
                                      <FormControl className="user-profile__postal custome-hight" type="text" placeholder="010106160" />

                                    </Col>
                                  </div>


                                </div>


                              </div>







                            </Row>





                          </div>

                        </Col>

                        <div className="user-profile__social-name clearfix">


                          <h3>{this.props.strings.social} </h3>
                          <hr />


                        </div>

                        <Col md={8} mdOffset={2}>
                          <div className="user-profile__social clearfix">
                            <Form>
                              <FormGroup>
                                <InputGroup>
                                  {/* <ControlLabel>{this.props.strings.fb}</ControlLabel> */}

                                  <InputGroup.Addon><i className="fa fa-facebook"></i> </InputGroup.Addon>
                                  <FormControl type="text" placeholder="Facebook URL" />
                                </InputGroup>

                                <InputGroup>
                                  {/* <ControlLabel>{this.props.strings.twitt}</ControlLabel> */}

                                  <InputGroup.Addon><i className="fa fa-twitter"></i> </InputGroup.Addon>
                                  <FormControl type="text" placeholder="Twitter URL" />
                                </InputGroup>

                                <InputGroup>
                                  {/* <ControlLabel>{this.props.strings.yout}</ControlLabel> */}

                                  <InputGroup.Addon><i className="fa fa-youtube"></i> </InputGroup.Addon>
                                  <FormControl type="text" placeholder="Youtube URL" />
                                </InputGroup>

                                <InputGroup>
                                  {/* <ControlLabel>{this.props.strings.insta}</ControlLabel> */}

                                  <InputGroup.Addon><i className="fa fa-instagram"></i> </InputGroup.Addon>
                                  <FormControl type="text" placeholder="Instagram URL" />
                                </InputGroup>


                              </FormGroup>
                            </Form>
                          </div>
                        </Col>
                      </Row>
                    </div>

                    <div className="order-empty_btn">

                      <Button className="user-profile__update order-empty__start" bsStyle="warning">{this.props.strings.upprof}</Button>
                    </div>

                  </div>
                </Col>
              </div>
            </Row>
            <hr />
          </div>


          <Row>



          </Row>

        </div>
      </Grid>

    );
  }
}

export default UserProfile;
