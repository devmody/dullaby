import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import ScrollAnimation from 'react-animate-on-scroll';

class NewArrive extends React.Component {
	
	render() {
      
		return (


        <div className="Newarrive">
           <Grid>
           <Row>
           <div className="Newarrive-holder">

                <div className="Newarrive-left">
                <Col md={6}>
               <h4>{this.props.strings.Arrivals} </h4>
               <p> {this.props.strings.Collection} </p> 
               <span>  <a href ="/shop">{this.props.strings.Lifestyle}</a> </span><span>  ... </span> 
                </Col>
                </div>
                <div className="Newarrive-right">

                <Col md={6}>
                <h4> </h4>

                <a href ="/shop"><h4>{this.props.strings.More} </h4></a>
                </Col>
                </div>

                </div>

       
     
         <div className="row Newarrive-mansory">
        
         
         <Col md={8}>    
              <Row>

         <Col md={12}>
         <ScrollAnimation animateIn="fadeInLeft"
			animateOnce={true}>
          <a href="/shop"><img src={require("../../img/Home/product1.png")}/> </a>
          </ScrollAnimation>

         </Col>
         </Row>
         <Row>

         <Col md={6}>
         <ScrollAnimation animateIn="fadeInUp"
		  	animateOnce={true}>
         <a href="/shop">  <img src={require("../../img/Home/product2.png")}/> </a>
         </ScrollAnimation>

         </Col>
         <Col md={6}>
         <ScrollAnimation animateIn="fadeInUp"
			animateOnce={true}>
         <a href="/shop"> <img src={require("../../img/Home/product3.png")}/> </a>
         </ScrollAnimation>

        </Col>  
        </Row>

         </Col>
         <Row>

         <Col md={4}>
         <ScrollAnimation animateIn="fadeInRight"
			animateOnce={true}>

         <a href="/shop"> <img src={require("../../img/Home/product4.png")}/></a>
         </ScrollAnimation>
        </Col>
        </Row>
     
        </div>
        </Row>

        </Grid>

  

    </div>

       );
    }
  }
  
  export default NewArrive;
  