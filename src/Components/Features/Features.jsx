import React from "react";
import { Row, Col } from 'react-bootstrap'

class Features extends React.Component {
	
	render() {
             
		return (


            <div className="container">
        <div className="features clearfix">
        
       <div className="features__item">
       <Col md={3}>
       <div className="features__item-img">
       <img src={require("../../img/Home/freeshipping.png")}/>
</div>
<div className="features__item-caption">

       <h3>{this.props.strings.shipping} </h3>
       <p> {this.props.strings.orders} </p>
       </div>
       </Col>
       </div>
       <div className="features__item">
       <Col md={3}>
       <div className="features__item-img">
       <img src={require("../../img/Home/return.png")}/>
</div>
<div className="features__item-caption">

       <h3>{this.props.strings.return} </h3>
       <p>{this.props.strings.proplem} </p>
       </div>
       </Col>
       </div>
       <div className="features__item">
       <Col md={3}>
       <div className="features__item-img">
       <img src={require("../../img/Home/securpayment.png")}/>
</div>
<div className="features__item-caption">

       <h3>{this.props.strings.payment} </h3>
       <p> {this.props.strings.secure}  </p>
       </div>
       </Col>
       </div>
       <div className="features__item">
       <Col md={3}>
       <div className="features__item-img">
       <img src={require("../../img/Home/support.png")}/>
</div>
<div className="features__item-caption">

       <h3> {this.props.strings.support}</h3>
       <p>{this.props.strings.Dedicated}</p>
       </div>
       </Col>
       </div>
       
        </div>
        </div>

       );
    }
  }
  
  export default Features;
  