import React from "react";
import { Row, Col, Button, Collapse, Well, Panel, PanelGroup } from 'react-bootstrap'
class HeaderTop extends React.Component {
constructor(props, context) {
super(props, context);
this.state = {
open: false
};
}
render() {
let image2 = require("../../img/Home/expand.png")
let image1 = require("../../img/Home/expand2.png")
return (
<div>
    <div className="ExpandCategoris">
        <PanelGroup onSelect={(x) =>
            this.setState({ selected: x })} accordion id="accordion-example">
            <Panel eventKey="1">
                <Panel.Body collapsible>
                    <div className="ExpandCategoris-holder">
                        <li className="ExpandCategoris-item">
                           <a href="/shop"> <img src={require("../../img/Home/women.png")}/></a>
                           <a href="/shop">    <h3> {this.props.strings.women} </h3> </a>
                        </li>
                        <li className="ExpandCategoris-item">
                            <img src={require("../../img/Home/men.png")}/>
                            <a href="/shop">   <h3> {this.props.strings.men} </h3> </a>
                        </li>
                        <li className="ExpandCategoris-item">
                        <a href="/shop"> <img src={require("../../img/Home/kids.png")}/></a>
                        <a href="/shop"> <h3>{this.props.strings.kids} </h3></a>
                        </li>
                    </div>
                </Panel.Body>
            </Panel>
            <Panel eventKey="1">
                <Panel.Heading>
                    <Panel.Title toggle><img src={this.state.selected === '1' ? (image1) : (image2)} />
                    </Panel.Title>
                </Panel.Heading>
            </Panel>
        </PanelGroup>
    </div>
</div>
);
}
}
export default HeaderTop;