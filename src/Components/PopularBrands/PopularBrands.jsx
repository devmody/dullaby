import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import ScrollAnimation from 'react-animate-on-scroll';

class PopularBrands extends React.Component {
	
	render() {
      
		return (


        <div className="Newarrive">
           <Grid>
           <Row>
           <div className="Newarrive-holder">

                <div className="Newarrive-left">
                <Col md={6}>
               <h4> {this.props.strings.brands} </h4>
               <p> {this.props.strings.fromcoll}  </p> 
              <a href="/shop"> <span> {this.props.strings.Nike} </span><span>  ... </span>  </a>
                </Col>
                </div>
                <div className="Newarrive-right">

                <Col md={6}>
                <h4> </h4>

                <h4>{this.props.strings.More}  </h4>
                </Col>
                </div>

                </div>

       
     
         <div className="row Newarrive-mansory">
        
         <ScrollAnimation animateIn="fadeInUp"
		  	animateOnce={true}>
         <Col md={3}>
         <Row>
         <Col md={12}>
        <a href="/shop"> <div className="popular-brand__item special-item one">
         <img src={require("../../img/Home/brand1.png")}/>
         <div className="new-item">
         <p>{this.props.strings.Neww} </p>
             </div>       
         <div className="popular-brand__item-name">
        
         <h3>{this.props.strings.swoosh}</h3>
         <div className="popular-brand__item-price">

         <p>{this.props.strings.price}</p>
         </div>

         </div>
         </div> </a>
         </Col>
         </Row>
         <Row>
         <Col md={12}>
         <a href="/shop"> <div className="popular-brand__item special-item one">
         <img src={require("../../img/Home/brand2.png")}/>
         <div className="popular-brand__item-name">

         <h3>{this.props.strings.Swoosh}</h3>
         <div className="popular-brand__item-price">

         <p>{this.props.strings.price}</p>
         </div>

         </div>
         </div> </a>
        </Col>  
        </Row>

         </Col>
         
         <Col md={6}>
         <Row>
         <Col md={12}>
         <a href="/shop">  <div className="popular-brand__item third-item two">
         <img src={require("../../img/Home/brand3.png")}/>
         <div className="popular-brand__item-name">

         <h3>{this.props.strings.Max} </h3>
         <div className="popular-brand__item-price">

         <p> <span className="was-price">{this.props.strings.pricemax} </span> {this.props.strings.pricemax} </p>
         </div>

         </div>
         </div> </a>
         </Col>
         </Row>

         </Col>
         <Col md={3}>
         <Row>
         
         <Col md={12}>
         <a href="/shop">   <div className="popular-brand__item special-item one">
         <img src={require("../../img/Home/brand4.png")}/>
         <div className="popular-brand__item-name">

         <h3> {this.props.strings.Swoosh}</h3>
         <div className="popular-brand__item-price">

         <p>{this.props.strings.price}</p>
         </div>

         </div>
         </div> </a>
         </Col>
         </Row>
         <Col md={12}>
         <Row>

         <a href="/shop">   <div className="popular-brand__item special-item one">
         <img src={require("../../img/Home/brand5.png")}/>
         <div className="popular-brand__item-name">

         <h3>{this.props.strings.Swoosh} </h3>
         <div className="popular-brand__item-price">

         <p>{this.props.strings.price}</p>
         </div>

         </div>
         </div> </a>
         </Row> 

        </Col> 

         </Col>
         </ScrollAnimation>
        </div>
        </Row>

        </Grid>

  

    </div>

       );
    }
  }
  
  export default PopularBrands;
  