import React from "react";
import { Row, Col, Grid, ToggleButton, ButtonToolbar, ToggleButtonGroup, Button } from 'react-bootstrap'
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import Quantity from "../Quantity/Quantity"
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Reviews from "../Reviews/Reviews";
import PopularProducts from "../PopularProducts/PopularProducts";
import TopRating from "../TopRating/TopRating";
import FeaturesProducts from "../FeaturesProducts/FeaturesProducts";
import Carousel from 'nuka-carousel';


class SingleProducts extends React.Component {

    render() {
        return (
            <div className="outfit singleproduct">
                <Breadcrumbing />
                <Grid>
                    <div className="singleproduct-bg">
                        <Row>


                            <Col md={12}>
                                <div className="outfit-desciption col-md-10 col-md-offset-1">
                                    <div className="singleproduct__title">
                                        <p>Autumn Outfit Casual Style For Men Lorem ipsum dolor sit amet.</p>
                                    </div>
                                    <div className="rating">
                                        <span>
                                            <ul>
                                                <li><a href="#"><i className="fa fa-star" /></a></li>
                                                <li><a href="#"><i className="fa fa-star" /></a></li>
                                                <li><a href="#"><i className="fa fa-star" /></a></li>
                                                <li><a href="#"><i className="fa fa-star" /></a></li>
                                                <li><a href="#"><i className="fa fa-star" /></a></li>
                                            </ul>
                                        </span>
                                        <span className="number-review"> (130 review) </span>
                                    </div>

                                    <div className="singleproduct__description">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
ut labore et dolore magna aliquat enim ad minim veniam.</p>
                                    </div>

                                    <div className="signel-discount">
                                        <div>
                                            <ul className="menu-price">
                                                <li>150.00 EGP</li>
                                                {/* <li className="was-price">200.00 EGP</li> */}
                                            </ul>
                                        </div>
                                        <div className="code">
                                            <p> Value-added tax INCLUDES </p>
                                        </div>
                                    </div>
                                </div>





                            </Col>

                            <Col md={12}>
                                <Carousel
                                    slidesToShow={1}
                                    renderCenterLeftControls={({ previousSlide }) => (
                                        <button className="left-arrow" onClick={previousSlide}><i class="fa fa-angle-left"></i></button>
                                    )}
                                    renderCenterRightControls={({ nextSlide }) => (
                                        <button className="right-arrow" onClick={nextSlide}><i class="fa fa-angle-right"></i>

                                        </button>
                                    )}>


                                    <div className="outfit-holder">

                                        <div className="finalsale__item-main">

                                            <a href="/singleshop"> <img src={require("../../img/outfit/outfit1.png")} /> </a>
                                        </div>


                                    </div>

        <div className="outfit-holder">

<div className="finalsale__item-main">

    <a href="/singleshop"> <img src={require("../../img/outfit/outfit1.png")} /> </a>
</div>


</div>



                                </Carousel>

                                <div className="outfit-contains">
                                    <div className="outfit-contains__title">

                                        <h3> Outfit Contains </h3>
                                    </div>
                                    <Row>
                                        <Col md={3}>
                                            <div className="oufit-cotainer__border">

                                                <div className="oufit-cotainer__image">

                                                    <img src={require("../../img/outfit/outfit-contain.png")} />


                                                </div>
                                                <div className="oufit-cotainer__content">

                                                    <h3>Levi’s Tshirt Rou </h3>
                                                    <p> Price : 230 L.E </p>
                                                    <h5>In Stock  </h5>
                                                    <img src={require("../../img/outfit/outfit-brand.png")} />
                                                    <div className="outfit-add__btn">

                                                        <a href="/cart"> <Button className="outfit-add__cart second-btn">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i> </Button> </a>

                                                    </div>
                                                </div>




                                            </div>
                                        </Col>
                                        <Col md={3}>
                                            <div className="oufit-cotainer__border">

                                                <div className="oufit-cotainer__image">

                                                    <img src={require("../../img/outfit/outfit-contain.png")} />


                                                </div>
                                                <div className="oufit-cotainer__content">

                                                    <h3>Levi’s Tshirt Rou </h3>
                                                    <p> Price : 230 L.E </p>
                                                    <h5>In Stock  </h5>
                                                    <img src={require("../../img/outfit/outfit-brand.png")} />
                                                    <div className="outfit-add__btn">

                                                        <a href="/cart"> <Button className="outfit-add__cart second-btn">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i> </Button> </a>

                                                    </div>
                                                </div>




                                            </div>
                                        </Col>

                                        <Col md={3}>
                                            <div className="oufit-cotainer__border">

                                                <div className="oufit-cotainer__image">

                                                    <img src={require("../../img/outfit/outfit-contain.png")} />


                                                </div>
                                                <div className="oufit-cotainer__content">

                                                    <h3>Levi’s Tshirt Rou </h3>
                                                    <p> Price : 230 L.E </p>
                                                    <h5>In Stock  </h5>
                                                    <img src={require("../../img/outfit/outfit-brand.png")} />
                                                    <div className="outfit-add__btn">

                                                        <a href="/cart"> <Button className="outfit-add__cart second-btn">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i> </Button> </a>

                                                    </div>
                                                </div>




                                            </div>
                                        </Col>
                                        <Col md={3}>
                                            <div className="oufit-cotainer__border">

                                                <div className="oufit-cotainer__image">

                                                    <img src={require("../../img/outfit/outfit-contain.png")} />


                                                </div>
                                                <div className="oufit-cotainer__content">

                                                    <h3>Levi’s Tshirt Rou </h3>
                                                    <p> Price : 230 L.E </p>
                                                    <h5>In Stock  </h5>
                                                    <img src={require("../../img/outfit/outfit-brand.png")} />
                                                    <div className="outfit-add__btn">

                                                        <a href="/cart"> <Button className="outfit-add__cart second-btn">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i> </Button> </a>

                                                    </div>
                                                </div>




                                            </div>
                                        </Col>
                                    </Row>

<hr className="outfit-hr"/>
                                </div>

                                <div className="single-buttons">

                                    <div className="single-quantit">
                                        <span>Product Quantity : </span>

                                        <Quantity />
                                    </div>

                                    <span>
                                        <a href="/cart"> <Button className="add-to-cart second-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i>Add to Cart </Button> </a></span>
                                        <a href="/wishlist"> <span className="wishlist-icon"><i class="fa fa-heart" aria-hidden="true"></i> </span></a>

                                        <span> 
                                        <ul className="headertop_menu">
                                        <li className="headertop-icon">
                                            <a href="#">
                                                <i className="fa fa-facebook"></i> </a>
                                        </li>
                                        <li className="headertop-icon">
                                            <a href="#">
                                                <i className="fa fa-twitter"></i> </a>
                                        </li>
                                        <li className="headertop-icon">
                                            <a href="#">
                                                <i className="fa fa-youtube"></i> </a>
                                        </li>
                                        <li className="headertop-icon">
                                            <a href="#">
                                                <i className="fa fa-instagram"></i> </a>
                                        </li>
                                    </ul>
                                    
                                        </span>

                                        


                         
                                </div>





                            </Col>



                        </Row>
                    </div>



                    <Row>
                        <Col md={3}>
                            <TopRating />
                            <FeaturesProducts />
                        </Col>

                        <Col md={9}>
                            <Reviews />
                        </Col>
                    </Row>

                </Grid>
                <PopularProducts />

            </div>

        );
    }
}
export default SingleProducts;