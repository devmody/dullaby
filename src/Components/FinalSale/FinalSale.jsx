import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import ScrollAnimation from 'react-animate-on-scroll';

class Finalsale extends React.Component {

render() {
    
  return (


      <div className="finalsale Newarrive">
               <ScrollAnimation animateIn="fadeInUp"
               			animateOnce={true}>
                     


          <Grid>
                      <Row>

          <div className="Newarrive-holder">

              <div className="Newarrive-left">
              <Col md={6}>
              <h4>{this.props.strings.Sale} </h4>
              <p>{this.props.strings.Extra} </p> 
              <span>{this.props.strings.Outfitss}  </span><span>  ... </span> 
              </Col>
              </div>
              <div className="Newarrive-right">

              <Col md={6}>
              <h4> </h4>

              <h4>{this.props.strings.Discover} </h4>
              </Col>
              </div>

              </div>


                <Carousel 
              slidesToShow={3}
      renderCenterLeftControls={({ previousSlide }) => (
        <button onClick={previousSlide}><i class="fa fa-angle-left"></i></button>
      )}
      renderCenterRightControls={({ nextSlide }) => (
        <button onClick={nextSlide}><i class="fa fa-angle-right"></i>

        </button>
      )}>
  
  
             <div className="finalsale__item">
              <div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish} </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div>
              <div className="finalsale__item-main">

             <a href="/singleshop"> <img src={require("../../img/Home/sale1.png")}/> </a>
              </div>
              <div className="finalsale__item-hover">
                <i class="fa fa-heart" aria-hidden="true"></i>
                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
               <a href="/singleshop"> <i class="fa fa-search" aria-hidden="true"></i> </a>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3>{this.props.strings.namehere}  </h3></a>
                  <span className="finalsale__was">{this.props.strings.fsale} </span>
                  <span className="finalsale__become">{this.props.strings.come} </span>

                </div>

              </div>

              

     
      
              <div className="finalsale__item">
              <div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish} </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div>
              <div className="finalsale__item-main">

             <a href="/singleshop">  <img src={require("../../img/Home/sale2.png")}/> </a>
              </div>
                <div className="finalsale__item-hover">
                <i class="fa fa-heart" aria-hidden="true"></i>
                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
               <a href="/singleshop"> <i class="fa fa-search" aria-hidden="true"></i> </a>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3>{this.props.strings.namehere}   </h3></a>
                  <span className="finalsale__was">{this.props.strings.fsale} </span>
                  <span className="finalsale__become">{this.props.strings.come} </span>

                </div>

              </div>

              

              <div className="finalsale__item">
              <div className="finalsale__item-wishlist">
              <h4>{this.props.strings.wish} </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div>
              <div className="finalsale__item-main">

             <a href="/singleshop"> <img src={require("../../img/Home/sale3.png")}/> </a>
              </div>
                <div className="finalsale__item-hover">
                <i class="fa fa-heart" aria-hidden="true"></i>
                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
               <a href="/singleshop"> <i class="fa fa-search" aria-hidden="true"></i> </a>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3>{this.props.strings.namehere}  </h3></a>
                  <span className="finalsale__was">{this.props.strings.fsale} </span>
                  <span className="finalsale__become">{this.props.strings.come} </span>

                </div>

              </div>

                 <div className="finalsale__item">
              <div className="finalsale__item-wishlist">
              <h4>200 </h4>
              <img src={require("../../img/Home/white-heart.png")}/>
              </div>
              <div className="finalsale__item-main">

             <a href="/singleshop"> <img src={require("../../img/Home/sale3.png")}/> </a>
              </div>
                <div className="finalsale__item-hover">
                <i class="fa fa-heart" aria-hidden="true"></i>
                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
               <a href="/singleshop"> <i class="fa fa-search" aria-hidden="true"></i> </a>
                </div>
                <div className="col-md-12 finalsale__item-details">
                  <a href="#"><h3>{this.props.strings.namehere} </h3></a>
                  <span className="finalsale__was">{this.props.strings.fsale} </span>
                  <span className="finalsale__become">{this.props.strings.come} </span>

                </div>

              </div>

              
      </Carousel>  



       
      
    
      
       </Row>

          </Grid>


 </ScrollAnimation>
                     
  </div>

      );
  }
}

export default Finalsale;
