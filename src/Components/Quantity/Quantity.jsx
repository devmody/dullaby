


import React from "react";
import { Row, Col, MenuItem, Nav, Navbar, NavItem, NavDropdown, Button, Panel, Grid, DropdownButton, FormGroup, FormControl } from 'react-bootstrap'
import InputNumber from 'rc-input-number';
import 'rc-input-number/assets/index.css';
class Quantity extends React.Component {
render() {
return (

    <div className="cart__quantity cell" data-title="Occupation">
                        <div>
                            <InputNumber
                                min={1}
                                max={10}
                                onChange={this.onChange}
                                defaultValue={1}
                                />
                        </div>
                    </div>


);
}
}
export default Quantity;


