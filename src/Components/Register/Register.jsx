import React from "react";
import { Row, Col, Grid ,FormGroup,Form,FormControl ,ControlLabel,Checkbox,MenuItem,DropdownButton,ButtonToolbar} from 'react-bootstrap'
import Signwith from "../Signwith/Signwith";

class Register extends React.Component {
	
	 render() {     
		return (

<div>
          
        <div class="Login Register">
        <Grid>
        <div className="Login-bg">
        <div className="Register_title">
        <h3>Register</h3>
         <p> Join Our Community </p>
         </div>

            <Row>
            <Col md={7} mdOffset={1}>

            
                <div className="Login-left">
                
                <Row>
                <Form horizontal>
                    <Col md={6}>
                    <FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="text" placeholder="Firstname" />
</FormGroup>
<FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="text" placeholder="Lastname" />
</FormGroup>

  <FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="password" placeholder="Password" />
</FormGroup>
<FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="password" placeholder="Confirm Password" />
</FormGroup>
                    </Col>
                    <Col md={6}>
                    <FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="text" placeholder="E-Mail" />
</FormGroup>

                 <ButtonToolbar>
    <DropdownButton title="Country" id="dropdown-size-medium">
      <MenuItem eventKey="1">Action</MenuItem>
      <MenuItem eventKey="2">Another action</MenuItem>
      <MenuItem eventKey="3">Something else here</MenuItem>
      <MenuItem divider />
      <MenuItem eventKey="4">Separated link</MenuItem>
    </DropdownButton>
  </ButtonToolbar>
     <ButtonToolbar>
    <DropdownButton title="City" id="dropdown-size-medium">
      <MenuItem eventKey="1">Action</MenuItem>
      <MenuItem eventKey="2">Another action</MenuItem>
      <MenuItem eventKey="3">Something else here</MenuItem>
      <MenuItem divider />
      <MenuItem eventKey="4">Separated link</MenuItem>
    </DropdownButton>
  </ButtonToolbar>
  <FormGroup controlId="formHorizontalEmail">
  
  <FormControl type="text" placeholder="Phone Number" />
</FormGroup>

                    </Col>
                    </Form>

                  </Row>

            
   
  </div>

                </Col>
                <Col md={4}>
                <FormGroup controlId="formHorizontalEmail">

                <div className="Login-right">
              

<a href="#"><button type="button" class="login_create btn btn-secondry">Create an account</button></a>
<div className="Login-content">
                <p> * After Registering, you will recieve an email  
with a verification Code to verify your account.</p>
</div>
                </div>
                </FormGroup>

                </Col>

                <div className="Login-hr clearfix">
                 <hr/>
                 </div>
                 <Signwith/>

             </Row>
</div>

        </Grid>

  </div>
       
        

      
        </div>

       );
    }
  }
  
  export default Register;
  