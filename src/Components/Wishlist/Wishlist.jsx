import React from "react";
import {Breadcrumb ,Col,Row,Grid} from 'react-bootstrap'
import Paginations from "../Paginations/Paginations";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import Features from "../Features/Features";
import ProductItemGrid from "../ProductItemGrid/ProductItemGrid";

class Wishlist extends React.Component {
render() {
return (
<div className="Wishlist">
<Grid>
<Breadcrumbing/>

<div className="Wishlist-bg">

<Row>

<Features/>
<hr/>
<Col md={3}>
       <ProductItemGrid/>
       </Col>
       <Col md={3}>
       <ProductItemGrid/>
       </Col>
       <Col md={3}>
       <ProductItemGrid/>
       </Col>
       <Col md={3}>
       <ProductItemGrid/>
       </Col>


<Paginations/>
</Row>
</div>
</Grid>

</div>
)
}
}
export default Wishlist;