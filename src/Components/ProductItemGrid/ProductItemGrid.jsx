import React from "react";
import { Row, Col ,MenuItem ,DropdownButton ,ButtonToolbar,Button,Grid } from 'react-bootstrap'
class ProductItemGrid extends React.Component {
render() {
return (
<div className="productItem">
    
        {/* product-wrapper-start */}
        <div className="product-wrapper mb-40">
            <div className="product-img">
                <a href="/singleshop">

                                  <img src={require("../../img/Product/1.jpg")}   alt="product" className="primary" />
                                  <img src={require("../../img/Product/1-1.jpg")}   alt="product" className="secondary" />


                
                </a>
                <span className="sale">{this.props.strings.salee}</span>
                <div className="product-icon">
                    <a href="#" data-toggle="tooltip" title data-original-title="Add to Cart"><i className="icon ion-bag" /></a>
                    <a href="#" data-toggle="tooltip" title data-original-title="Compare this Product"><i className="icon ion-android-options" /></a>
                    <a href="#" data-toggle="modal" data-target="#mymodal" title="Quick View"><i className="icon ion-android-open" /></a>
                </div>
            </div>
            <div className="product-content pt-20">
                <div className="name-product">
                    <h2><a href="#">{this.props.strings.corporate}</a></h2>
                    <div className="price">
                        <div className="rating">
                            <span>
                                <ul>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                    <li><a href="#"><i className="fa fa-star" /></a></li>
                                </ul>
                            </span>
                            <span className="number-review">{this.props.strings.rev} </span>
                        </div>
                        <ul className="menu-price">
                            <li className="new-price">{this.props.strings.was}</li>
                            <li className="was-price"> {this.props.strings.new}</li>
                        </ul>
                        <span>  <a href="/cart">   <Button  className="add-to-cart second-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i>{this.props.strings.cartt} </Button> </a></span>
                        <a href="/wishlist"> <span className="wishlist-icon"><i class="fa fa-heart" aria-hidden="true"></i> </span></a>
                    </div>
                </div>
            </div>
        </div>
        
        </div>
        
    
     
);
}
}
export default ProductItemGrid  ;