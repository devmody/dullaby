import React from "react";
import { Row, Col, Grid ,FormGroup,Form,FormControl ,ControlLabel,Checkbox,MenuItem,DropdownButton,ButtonToolbar} from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";

class WishlistEmpty extends React.Component {
	
	 render() {     
		return (

            <Grid>
            <Breadcrumbing/>

   <div className="WishlistEmpty  cartempty">
   <Features/>
   <hr/>

<Row>
            <Col md={8} mdOffset={2}>

       <h3 className="emptytitle">Unfortunately, your wish list is empty</h3>
       <Col md={6}>

       <img  class="emptycart__img" src={require("../../img/cart/wishlist-empty.png")} />
       </Col>
       <br/>

         <Col md={6}>
         <div className="WishlistEmpty__btns">

       <a href="#"><button type="button" class="empty-cart__continue btn ">Continue Shopping</button></a>
       <br/>
       <a href="/"><button type="button" class="empty-cart__start btn">Discover Popular Products</button></a>
       </div>
       </Col>

          </Col>
          </Row>
          
          </div>

          </Grid>

       );
    }
  }
  
  export default WishlistEmpty;
  