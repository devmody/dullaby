import React from "react";
import { Row, Col } from 'react-bootstrap'
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
// import '../../../css/slider-animations.css';
import './styles.css';
import ScrollAnimation from 'react-animate-on-scroll';


const content = [
	{
		userScrollTop: require("../../img/Home/scroll.png"),
		description:
		' • MEN  Category •',
		image: require("../../img/Home/slider1-1.jpg"),
		user: 'Luan Gjokaj',
		userScroll: require("../../img/Home/scroll.png"),
		userResponsive: require("../../img/Home/responsive.png"),
		userKeyboard: require("../../img/Home/keybaord.png"),


	},
	{
		title: 'Summer Outfit for Women',
		userScrollTop: require("../../img/Home/scroll.png"),
		description:
		' • Women  Category •',
		image: require("../../img/Home/slider1-2.jpg"),
		user: 'Erich Behrens',
		discover:'Discover More...',
		userScroll: require("../../img/Home/scroll.png"),
		userResponsive: require("../../img/Home/responsive.png"),
		userKeyboard: require("../../img/Home/keybaord.png"),	},
	
];



class HeaderSlider extends React.Component {
	
	render() {
		return (

		<ScrollAnimation animateIn="fadeIn"
			animateOnce={true}>

		<div className="headersliderx">
   <Slider className="slider-wrapper container-fluid">
			{content.map((item, index) => (
				<div
					key={index}
					className="slider-content"
					style={{ background: `url('${item.image}') no-repeat center top` }}
				>
					<div className="inner">
						<h1>{this.props.strings.itemtitle}</h1>
						<p>{this.props.strings.itemtitle}</p>

						<div className="user_scroll">
						<a href="#"><img src={item.userScrollTop} alt={item.user} /> </a>

						</div>
						<h4>{this.props.strings.discover}</h4>
					</div>
					<section>
						<a href="#"><img src={item.userScroll} alt={item.user} /> </a>
						<a href="#"><img src={item.userResponsive} alt={item.user} /> </a>
						<a href="#"><img src={item.userKeyboard} alt={item.user} /> </a>


					</section>
				</div>
				
			))}
		</Slider>


      </div>
	  </ScrollAnimation>

       );
    }
  }
  
  export default HeaderSlider;
  