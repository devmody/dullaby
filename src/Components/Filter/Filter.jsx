import React from "react";
import { Row, Col ,MenuItem ,DropdownButton ,ButtonToolbar,Button,Checkbox } from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import TopRating from "../TopRating/TopRating";
import FeaturesProducts from "../FeaturesProducts/FeaturesProducts";

class Filter extends React.Component {
render() {
return (
<div className="">
   <div className="product-filter">
      <div className="product-filter-name_holder">
      <span className="product-filter-name">  
      <img src={require("../../img/Product/filter-category.png")} /> 
      </span>
      <span className="product-filter-name">{this.props.strings.Filtercat}  </span>
      </div>

      <ul class="product-filter__menu product-categorie">
         <li className="active-category"><a href="#">{this.props.strings.mencategory}  </a></li>
         <hr/>
         <li><a href="#">{this.props.strings.womencategory}</a></li>
         <hr/>
         <li><a href="#">{this.props.strings.kidscategory}</a></li>
      </ul>
      <div class="product-filter__menu shop-categorie">
         <span className="product-filter-name">{this.props.strings.shopcategory} </span>
         <div className="product-filter__menu shop-type">
            <h3>{this.props.strings.size} </h3>
         </div>
         <Checkbox>{this.props.strings.smallsize}</Checkbox>

         <Checkbox>{this.props.strings.medsize}</Checkbox>
         <Checkbox>{this.props.strings.ssize}</Checkbox>
         <Checkbox>{this.props.strings.large}</Checkbox>
      </div>
      <div class="product-filter__menu shop-categorie">
         <div className="product-filter__menu shop-type">
            <h3>{this.props.strings.pstyle} </h3>
         </div>
         <Checkbox>{this.props.strings.clsstyle}</Checkbox>
         <Checkbox>{this.props.strings.cagstyle}</Checkbox>
         <Checkbox>{this.props.strings.bigstyle}</Checkbox>
         <Checkbox>{this.props.strings.flatstyle}</Checkbox>
         <Checkbox>{this.props.strings.advstyle}</Checkbox>
      </div>
      <div class="product-filter__menu shop-categorie">
         <div className="product-filter__menu shop-type">
            <h3>{this.props.strings.fet}</h3>
         </div>
         <Checkbox>{this.props.strings.egy}</Checkbox>
         <Checkbox>{this.props.strings.ek}</Checkbox>
      </div>
     
      <div class="product-filter__menu shop-categorie">
         <div className="product-filter__menu shop-type">
    
            <h3>{this.props.strings.brandss} </h3>
            <input type="text" placeholder= {this.props.strings.search} id="formControlsText" class="form-control"/>
         <Checkbox>{this.props.strings.calvin}</Checkbox>
         <Checkbox>{this.props.strings.zara}</Checkbox>
         <Checkbox>{this.props.strings.polo}</Checkbox>
      </div>
      </div>

      <div class="product-filter__menu shop-categorie">
         <div className="product-filter__menu shop-type">
            <h3>{this.props.strings.procolor}</h3>
         </div>
         <div className="product-filter__menu-color">
            <div className="color-item custome">
               <a href="#"> </a>
            </div>
            <h3>{this.props.strings.prored}  </h3>
         </div>
      </div>
      <div class="product-filter__menu shop-categorie">
         <div className="product-filter__menu shop-type">
            <h3> {this.props.strings.toprates}</h3>
         </div>
         <Checkbox>{this.props.strings.fife}</Checkbox>
         <Checkbox>{this.props.strings.four}</Checkbox>
         <Checkbox>{this.props.strings.three}</Checkbox>
      </div>
     
      

      <TopRating {...this.props}/>
      <FeaturesProducts {...this.props}/>

               
    
   </div>
</div>
);
}
}
export default Filter;