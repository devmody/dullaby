import React from "react";
import { Row, Col , Grid,PageHeader } from 'react-bootstrap'
import Carousel from 'nuka-carousel';

class TopRating extends React.Component {

render() {
    
  return (


     <div>
  {/* Start feature-prodct from here */}
   <div class="product-filter__menu top-rating">
      <div className="product-filter-name_holder">
      <span className="product-filter-name">  
      <img src={require("../../img/Product/top-rating.png")} /> 
      </span>
      <span className="product-filter-name">{this.props.strings.topproduct}  </span>
      </div>
  <div class="row feature-prodct__item item top-sale">
            <div class="col-md-4">
               <div class="product-image-hover">
                  <a href="#">
                  <img class="primary-image" src={require("../../img/Product/shirt2.png")} />
                  </a>
               </div>
            </div>
            <div class="col-md-8">
               <div class="dsds">
                  <div class="featured-product__text">
                     <h4><a href="#">{this.props.strings.shirtctn}</a>
                     </h4>
                  </div>
                  <div class="feature-product__rating">
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                  </div>
                  <div class="product"><span class="price_holder">{this.props.strings.priceee}</span>
                  </div>
               </div>
            </div>
         </div>
         <hr/>
               {/* End feature-prodct from here */}


 {/* Start feature-prodct from here */}
 <div class="row feature-prodct__item item top-sale">
            <div class="col-md-4">
               <div class="product-image-hover">
                  <a href="#">
                  <img class="primary-image" src={require("../../img/Product/shirt2.png")} />
                  </a>
               </div>
            </div>
            <div class="col-md-8">
               <div class="dsds">
                  <div class="featured-product__text">
                     <h4><a href="#">{this.props.strings.shirtctn}</a>
                     </h4>
                  </div>
                  <div class="feature-product__rating">
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                     <i class="fa fa-star"></i>
                  </div>
                  <div class="product"><span class="price_holder">{this.props.strings.priceee} </span>
                  </div>
               </div>
            </div>
         </div>
         <hr/>
               {/* End feature-prodct from here */}

  </div>
  </div>

      );
  }
}

export default TopRating;
