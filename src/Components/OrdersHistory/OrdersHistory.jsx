import React from "react";
import { ListGroupItem, ListGroup, Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Button, Tab, NavItem, Nav } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import UserSetting from "../UserSetting/UserSetting";
import Quantity from "../Quantity/Quantity";
import InputNumber from 'rc-input-number';

import Paginations from "../Paginations/Paginations";


class OrdersHisrory extends React.Component {
    render() {
        return (
            <Grid>
<div className="orders-history">
                <div className="payment__empty">
                    <Row>
                        <UserSetting />
                        <div className="text">
                            <Col md={9}>
                                <div className="payment__method-bg">
                                    <div>
                                        <div className="Payment__methods__title">
                                            <h3> Orders History</h3>
                                        </div>

                                    </div>
                                        
     <table>
    <thead>
      <tr className="orders-history__head">
        <th>Order Number </th>
        <th>Order Date </th>
        <th>Items </th>
        <th>  Total Price </th>
        <th> Status </th>

        <th>  Action</th>

       
      </tr>
    </thead>
    <tbody>
    <tr>
    <td data-label="Order Number">1270009</td>
     <td data-label="Order Date ">July 27, 2018</td>
     <td data-label="Items ">6 items</td> 
     <td data-label=" Total Price">1000 EGP</td> 
     <td data-label="Status">Complete</td> 
     <td data-label="Action"><a  className="orders-history__action" href="/orderrecived">Details</a></td> 


    </tr>
    <tr>
    <td data-label="Order Number">1270009</td>
     <td data-label="Order Date ">July 27, 2018</td>
     <td data-label="Items ">6 items</td> 
     <td data-label=" Total Price">1000 EGP</td> 
     <td data-label="Status">Complete</td> 
     <td data-label="Action"><a  className="orders-history__action" href="/orderrecived">Details</a></td> 


    </tr>
    <tr>
    <td data-label="Order Number">1270009</td>
     <td data-label="Order Date ">July 27, 2018</td>
     <td data-label="Items ">6 items</td> 
     <td data-label=" Total Price">1000 EGP</td> 
     <td data-label="Status">Complete</td> 
     <td data-label="Action"><a  className="orders-history__action" href="/orderrecived">Details</a></td> 


    </tr>
    <tr>
    <td data-label="Order Number">1270009</td>
     <td data-label="Order Date ">July 27, 2018</td>
     <td data-label="Items ">6 items</td> 
     <td data-label=" Total Price">1000 EGP</td> 
     <td data-label="Status">Complete</td> 
     <td data-label="Action"><a  className="orders-history__action" href="/orderrecived">Details</a></td> 


    </tr>
    </tbody>
  </table>
  <Paginations/>


                                </div>

                            </Col>
                        </div>
                    </Row>
                    <hr />
                </div>
                <Features />
                </div>

            </Grid>

        );
    }
}
export default OrdersHisrory;