import React from "react";
import { Row, Col, ToggleButton, ToggleButtonGroup,ButtonToolbar  } from 'react-bootstrap'
class HeaderTop extends React.Component {
render() {
return (
<div className="headertop">
    <div className="container">
<Row>

        <Col md={4}>
        <div className="languages">
            <ButtonToolbar className="Language-choose">
     <ToggleButtonGroup onChange={(value) => this.props.onLangChange(value)} type="radio" name="options" defaultValue={'en'}>
       <ToggleButton id="englishBtn" value={'en'}>English</ToggleButton>
       <ToggleButton id="arabicBtn" value={'ar'}>عربى</ToggleButton>                
     </ToggleButtonGroup>
   </ButtonToolbar>
   </div>
        <ul className="headertop_menu">
            <li className="headertop-icon">
                <a  href="https://www.facebook.com/DollabyEgypt/" target="_blank">
                <i className="fa fa-facebook"></i> </a>
            </li>
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-twitter"></i> </a>
            </li>
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-youtube"></i> </a>
            </li>
            <li className="headertop-icon">
                <a href="#">
                <i className="fa fa-instagram"></i> </a>
            </li>
        </ul>
        </Col>
        <Col md={5}>
        <div className="headertop_logo">
            <a href="/"><img src={require("../../img/Home/logo.png")}/>  </a> 
        </div>
        </Col>
        <Col md={3}>
        <div className="headertop_right">
            <div class="header-top__holder">
                <div className="icon-addon addon-lg">
                   <a href="#"> <input type="text" placeholder="Search.." className="form-control" id="email" /> </a>
                     <a href="#"><label for="email" className="glyphicon glyphicon-search" rel="tooltip" title="email" /> </a>
                </div>
            </div>
            <ul className="headertop_menu right">
                <li className="headertop-icon">
                    <a href="/Wishlist">
                    <img src={require("../../img/Home/wishlist.png")} />
                    </a>
                </li>
                <li className="headertop-icon">
                    <a href="/login">
                    <img src={require("../../img/Home/user.png")} />
                    </a>
                </li>
                <li className="headertop-icon">
                    <a href="/cart">
                    <img src={require("../../img/Home/bag.png")} />
                    </a>
                </li>
            </ul>
        </div>
        </Col>
        </Row>

    </div>
</div>
);
}
}
export default HeaderTop;