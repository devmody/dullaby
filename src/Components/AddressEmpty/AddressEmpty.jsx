import React from "react";
import { Row, Col ,Grid  , Button} from 'react-bootstrap'
import Carousel from 'nuka-carousel';
import UserSetting from "../UserSetting/UserSetting";

class AddressEmpty extends React.Component {
	 
	render() {
      
		return (



            <Grid>




            <div className="Address-empty order-empty payment__empty">
                <Row>

                    <UserSetting {...this.props}/>
                    <div className="text">
                        <Col md={9}>
                            <div className="payment__method-bg">
                                <div>
                                    <div className="Payment__methods__title">
                                        <h3> Shipping Address Information</h3>
                                    </div>
 
                                    <hr className="Payment__methods-hr" />
                                <Row>

                                    <Col md={8} mdOffset={2}>
                                    <div className="order-history__content">


                   <img src={require("../../img/Address/car.png")} />
                   <div className="order-empty_btn">

                     <Button className="Address-empty order-empty__start" bsStyle="warning">Add New Shipping Address</Button>
</div>

                                    </div>

                                    </Col>
                                </Row>
                            </div>
                            </div>
                        </Col>
                    </div>
                </Row>
                <hr />
            </div>
            {/* <Features /> */}
        </Grid>

       );
    }
  }
  
  export default AddressEmpty;
  