import React from "react";
import { ListGroupItem, ListGroup, Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Button,Tab,NavItem,Nav } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import UserSetting from "../UserSetting/UserSetting";
class AddPayment extends React.Component {
render() {
return (
<Grid>




    <div className="Add-payment__method">
        <Row>
            <UserSetting {...this.props}/>
            <div className="text">
            <Col md={9}>
            <div className="payment__method-bg">
            <div>
                <div className="Payment__methods__title">
                    <h3>{this.props.strings.method}</h3>
                </div>
             
               
                <hr className="Payment__methods-hr"/>
            </div>
            <Row>
            <Col md={8} mdOffset={2}>

              

                            <div className="checkout-title">
                                
                            

 <Tab.Container id="left-tabs-example" defaultActiveKey="first">
  <Row>
    <Col sm={12}>
      <Nav bsStyle="pills" stacked>
        <NavItem eventKey="first">  
         <img title="1" src={require("../../img/checkout/credit.png")} />
<p>{this.props.strings.cridt}</p></NavItem>
        <NavItem eventKey="second"><img title="1" src={require("../../img/checkout/paypal.png")} />
<p>{this.props.strings.pypl}</p></NavItem>


   <NavItem eventKey="third"><img title="1" src={require("../../img/payment/cashondelevry.png")} />
<p>{this.props.strings.cash} </p></NavItem>
      </Nav>
      
    </Col>
    <Row>
    <Col sm={12}>
      <Tab.Content animation>
      <hr/>

        <Tab.Pane eventKey="first">
        
     
                                        <Col md={12}>

                                            <FormGroup controlId="formHorizontalEmail">

                                                <FormControl type="text" placeholder="Card Number" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={9}>

                                            <FormGroup controlId="formHorizontalEmail">

                                                <FormControl className="card-Holder" type="text" placeholder="Card Holder*" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={3}>

                                            <FormGroup controlId="formHorizontalEmail">

                                                <FormControl className="user-cvv" type="text" placeholder="CVV*" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>

                                            <DropdownButton title={this.props.strings.exmonth} id="ExpireMonth">
                                                <MenuItem eventKey="1">{this.props.strings.action}</MenuItem>
                                                <MenuItem eventKey="2">{this.props.strings.anotaction}</MenuItem>
                                                <MenuItem eventKey="3">{this.props.strings.someelse}</MenuItem>
                                                <MenuItem divider />
                                                <MenuItem eventKey="4">{this.props.strings.separated}</MenuItem>
                                            </DropdownButton>
                                        </Col>

                                        <Col md={6}>

                                            <DropdownButton title={this.props.strings.exyear} id="ExpireYear">
                                            <MenuItem eventKey="1">{this.props.strings.action}</MenuItem>
                                                <MenuItem eventKey="2">{this.props.strings.anotaction}</MenuItem>
                                                <MenuItem eventKey="3">{this.props.strings.someelse}</MenuItem>
                                                <MenuItem divider />
                                                <MenuItem eventKey="4">{this.props.strings.separated}</MenuItem>
                                            </DropdownButton>
                                        </Col>
                                        <Col md={12}>
                                        <div className="checkout-payment">
                                            <img src={require("../../img/checkout/payment.png")} />
</div>
                                        </Col>
                                        
                                        </Tab.Pane>
        <Tab.Pane eventKey="second">{this.props.strings.conttwo} </Tab.Pane>

           <Tab.Pane eventKey="third">{this.props.strings.contthre}</Tab.Pane>

      </Tab.Content>
      <Col md={12}>

 
</Col>
    </Col>
    </Row>
 
 
  </Row>



</Tab.Container>

                            </div>
                            
                                 <div className="Add-payment__method-buttons">
    <span><a href="#"><button type="button" className="Add-payment__method btn ">{this.props.strings.addpaymen}</button></a> </span>
       
      <span> <a href=""><button type="button" className="Add-payment__cancel-process btn">{this.props.strings.process}</button></a> </span>
</div>
                            </Col>
                            </Row>

                        </div>

            </Col>
            </div>
        </Row>
        <hr/>
    </div>
    <Features {...this.props}/>
</Grid>

);
}
}
export default AddPayment;