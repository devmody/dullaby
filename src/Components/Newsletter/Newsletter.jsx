import React from "react";
import { Row, Col } from 'react-bootstrap'

class Newsletter extends React.Component {
	
	render() {
             
		return (


        <div className="newsletter">
         <div className="container">
         <Row>
         <div className="newsletter__caption">
         
         <Col md={6}>
         <h3> {this.props.strings.Subscribe} </h3>
         <p>{this.props.strings.informed}  </p>
         </Col>
         </div>
         <Col md={6}>
         <form id="register-newsletter">
                     <input type="text" name="newsletter" required="" placeholder= {this.props.strings.mail}/>
                        <input type="submit" className="btn btn-custom-3" value={this.props.strings.sign}/>
         </form>
         </Col>


      </Row>
        </div>
        </div>

       );
    }
  }
  
  export default Newsletter;
  