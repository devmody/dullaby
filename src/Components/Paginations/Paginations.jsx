import React from "react";
import {Pagination,Pager } from 'react-bootstrap'

class Paginations extends React.Component {
render() {
return (
<div className="Paginations">
<Pagination>
<Pager.Item previous href="#">
{this.props.strings.vious}
  </Pager.Item>

  <Pagination.Item active>{1}</Pagination.Item>
  <Pagination.Item>{2}</Pagination.Item>
  <Pagination.Item>{3}</Pagination.Item>
  <Pagination.Item>{4}</Pagination.Item>
  <Pagination.Item >{5}</Pagination.Item>
 
  <Pagination.Item>{6}</Pagination.Item>
  <Pagination.Ellipsis />

  <Pager.Item  next href="#">
  {this.props.strings.next}
  </Pager.Item>
</Pagination>
</div>
);
}
}
export default Paginations;