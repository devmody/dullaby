import React from "react";
import { Breadcrumb,Grid } from 'react-bootstrap'

class Breadcrumbing extends React.Component {
render() {
return (
  <Grid>
<div className="Breadcrumb-holder">
<Breadcrumb>
  <Breadcrumb.Item href="/">{this.props.strings.home}</Breadcrumb.Item>
 
  <Breadcrumb.Item active>{this.props.strings.shopp}</Breadcrumb.Item>
</Breadcrumb>
</div>
</Grid>
  
)
}
}
export default Breadcrumbing;