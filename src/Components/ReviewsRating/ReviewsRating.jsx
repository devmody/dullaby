import React from "react";
import { ListGroupItem, ListGroup, Row, Col, Grid, FormGroup, Form, FormControl, ControlLabel, Checkbox, MenuItem, DropdownButton, ButtonToolbar, Button, Tab, NavItem, Nav } from 'react-bootstrap'
import Features from "../Features/Features";
import Breadcrumbing from "../Breadcrumbing/Breadcrumbing";
import UserSetting from "../UserSetting/UserSetting";
import  Rating  from 'react-rating';
import '../../css/font-awesome.min.css'

class ReviewsRating extends React.Component {
    render() {
        return (
            <div className="Reviews-Rating">
                <Grid>
                    <Row>

                        <UserSetting />
                        <Col md={9}>
                        <div className="Reviews-Rating__bg">
                            <div className="Reviews-Rating__title">
                                <h3>Reviews & Rating</h3>
                            </div>
                            <div className="Reviews-Rating__sort">
                                <span className="Reviews-Rating__sortby">Sorting By : </span>
                                <span> 
                                    <DropdownButton title="Default button" id="dropdown-size-medium">
                                        <MenuItem eventKey="1">Action</MenuItem>
                                        <MenuItem eventKey="2">Another action</MenuItem>
                                        <MenuItem eventKey="3">Something else here</MenuItem>
                                        <MenuItem divider />
                                        <MenuItem eventKey="4">Separated link</MenuItem>
                                    </DropdownButton>
                               
                                </span>
                            </div>
                        <hr className="Reviews-Rating-hr"/>


                            <Row>

                                <Col md={4}>


                                    <div className="Reviews-Rating__item-image">
                                        <img src={require("../../img/review/shirt.png")} />
                                    </div>

                                    <div className="Reviews-Rating__item">
                                        <h4>Casual Shirt Cotton <br></br>New 2018 Spring</h4>
                                        <h5>150.00 EGP</h5>
                                        <p>Category : Men -Shirts</p>


                                    </div>
                                    <span>
                                        <i class="material-icons">
                                            delete_forever
                                            </i>
                                            </span>
                        
                        <div className="Reviews-Rating__delete">

                                        <Button bsStyle="primary">Edit Review</Button>
                                        
                                    </div>


                                </Col>

                                <Col md={8}>
                                <div className="Reviews-Rating__Comment">
                               <span className="Reviews-Rating__Stars"> <Rating
  emptySymbol="fa fa-star-o fa-2x"
  fullSymbol="fa fa-star fa-2x"
/>
</span>
<span className="Reviews-Rating__Date"> Added on : 14-11-206 11:23 PM </span>
<p> cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt </p>

</div>


                                </Col>

                            </Row>



</div>

                        </Col>






                    </Row>


                </Grid>
            </div>

        );
    }
}
export default ReviewsRating;