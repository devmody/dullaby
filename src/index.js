import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom'
import './css/bootstrap.min.css'
import './css/style.css'
import './css/responsive.css'
import './css/font-awesome.min.css'
import "animate.css/animate.min.css";

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
, document.getElementById('root'));

